## Guild Wars 2 Java Api Wrapper ##

### About ###

This is a comprehensive API wrapper for the [Guild Wars 2 API](https://wiki.guildwars2.com/wiki/API:2) (version 2 specifically). What I mean by comprehensive is that by the end of the project, every Guild Wars 2 object in the API will have a corresponding Java Object in the project as well as a means to access the corresponding endpoint of that object. 

This is done to take out the tedious tasks that Guild Wars 2 application developers have to do when creating API's (creating methods that hit an endpoint, creating objects that correspond to the JSON object), and focus more on the meat of their project (using the data acquired for their specified task). Thus doing the task of any good API.

### Why Exist ###

1. Most Guild Wars 2 API's are incomplete or have design decisions that I particularly disagree with. Many don't provide the objects (including the Java ones). This will aim to one day be complete and comprehensive in covering all the Guild wars 2 Objects. 
2. Practice and a project that I'd been low key interested in doing for a while. I used to play the game, but have long since quit. The project also gets me to refresh a lot of skills that I'd not practiced in Graduate School (JSON Mapping, Large Scale projects with maven, lots and lots of unit tests, and more) so it was a good refresher before going back into industry. So good practice and motivation to do so is a good combination.

### How To Use ##

Download the repository (either clone it or download the zip file). Once done build it using

	mvn clean install
    
Once that is complete, import the maven modules that you need. If you need an object, you should only need to import the specified object module. If you want to hit an endpoint, you need to get the corresponding wrapper.

### To Do ###

Currently the only complete and tested Wrappers thus far are:

* Items
* Dungeons
* Finishers

Currently in progress is:

* Achievements

Thus leaving a lot of Guild Wars 2 API incomplete. This leaves the todo task as follows:

* Finish the rest of the API.
* More Tests
* Refactor some designs and tests. 