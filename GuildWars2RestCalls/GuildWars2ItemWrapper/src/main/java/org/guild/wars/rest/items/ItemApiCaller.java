package org.guild.wars.rest.items;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.guild.wars.objects.items.Item;
import org.guild.wars.rest.common.GuildWars2ApiCaller;
import org.guild.wars.rest.common.HTTPManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 
 * @author Gab
 * TODO: Bug fix. Item API caller currently can get an illegal
 * item and doesn't indicate this to the user (e.g. the item id doesn't exist).
 */

public class ItemApiCaller extends GuildWars2ApiCaller
{
	private final static String ITEM_ENDPOINT = "https://api.guildwars2.com/v2/items";
	private ObjectMapper objectMapper;
	
	public ItemApiCaller(HTTPManager httpManager, String apiKey, ObjectMapper objectMapper)
	{
		super(httpManager, apiKey);
		this.objectMapper = objectMapper;
	}
	
	public Item getItem(String id) throws JsonParseException, JsonMappingException, IOException
	{
		String url = ITEM_ENDPOINT + "/" + id;
		String jsonItem = this.getHTTPManager().getEndpoint(url);
		Item item = null;

		item = objectMapper.readValue(jsonItem, Item.class);
		
		if(item == null || item.getName() == null)
			throw new IOException();
		
		return item;
	}
	
	public Item[] getItem(String... idArray) throws JsonParseException, JsonMappingException, IOException
	{
		Item[] itemOut = new Item[idArray.length];
		String ids = this.buildIdParams(idArray);
		String[] params = {ids};
		String jsonItems = this.getHTTPManager().getEndpoint(ITEM_ENDPOINT, params);

		itemOut = objectMapper.readValue(jsonItems, Item[].class);
		
		return itemOut;
	}
	
	public Item getItem(String id, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		String url = ITEM_ENDPOINT + "/" + id;
		String jsonItem = this.getHTTPManager().getEndpoint(url, params);
		Item item = null;
	
		item = objectMapper.readValue(jsonItem, Item.class);
		if(item == null || item.getName() == null)
			throw new IOException();
		
		return item;
	}
	
	public Item[] getItem(String[] idArray, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		Item[] itemOut = new Item[idArray.length];
		String ids = this.buildIdParams(idArray);
		String[] idsParams = {ids};
		String[] paramsUnion = ArrayUtils.addAll(params, idsParams);
		String jsonItems = this.getHTTPManager().getEndpoint(ITEM_ENDPOINT, paramsUnion);
		
		itemOut = objectMapper.readValue(jsonItems, Item[].class);

		return itemOut;
	}
	
	
	/**
	 * Get multiple items, each with their own unique parameters. 
	 * @param idArray The list of items to fetch. 
	 * @param paramList Must be the same length as idArray
	 * @return An array of {@link Item}
	 */
	public Item[] getItem(String[] idArray, List<String[]> paramList)
	{
		Item[] itemOut = new Item[idArray.length];
		for(int i = 0; i< idArray.length; i++)
		{
			Item item = new Item();
			try
			{
				item = this.getItem(idArray[i], paramList.get(i));
			} 
			catch (IOException e)
			{
				System.out.println("Bad item id: " + idArray[i]);
				e.printStackTrace();
			}
			itemOut[i] = item;
		}
		return itemOut;
	}
	
	public Item getAuthItem(String id) throws JsonParseException, JsonMappingException, IOException
	{
		String url = ITEM_ENDPOINT + "/" + id;
		String jsonItem = this.getHTTPManager().getEndpoint(url, this.getApiKey());
		Item item = null;
		
		item = objectMapper.readValue(jsonItem, Item.class);
		
		if(item == null || item.getName() == null)
			throw new IOException();
	
		return item;
	}
	
	public Item[] getAuthItem(String... idArray) throws JsonParseException, JsonMappingException, IOException
	{
		Item[] itemOut = new Item[idArray.length];
		String ids = this.buildIdParams(idArray);
		String[] params = {ids};
		String jsonItems = this.getHTTPManager().getEndpoint(ITEM_ENDPOINT, this.getApiKey(), params)
				;
		itemOut = objectMapper.readValue(jsonItems, Item[].class);
		
		return itemOut;
	}
	
	public Item getAuthItem(String id, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		String url = ITEM_ENDPOINT + id;
		String jsonItem = this.getHTTPManager().getEndpoint(url, this.getApiKey(), params);
		Item item = null;
		
		item = objectMapper.readValue(jsonItem, Item.class);

		if(item == null || item.getName() == null)
			throw new IOException();
	
		return item;
	}
	
	public Item[] getAuthItem(String[] idArray, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		Item[] itemOut = new Item[idArray.length];
		String ids = this.buildIdParams(idArray);
		String[] idsParams = {ids};
		String[] paramsUnion = ArrayUtils.addAll(params,idsParams);
		String jsonItems = this.getHTTPManager().getEndpoint(ITEM_ENDPOINT, this.getApiKey(), paramsUnion);

		itemOut = objectMapper.readValue(jsonItems, Item[].class);

		return itemOut;
	}

	
	/**
	 * Get multiple items, each with their own unique parameters
	 * from the authenticated endpoints.
	 * @param idArray The list of items to fetch. 
	 * @param paramList Must be the same length as idArray
	 * @return An array of {@link Item}
	 */
	public Item[] getAuthItem(String[] idArray, List<String[]> paramList)
	{
		Item[] itemOut = new Item[idArray.length];
		for(int i = 0; i< idArray.length; i++)
		{
			Item item = new Item();
			try
			{
				item = this.getAuthItem(idArray[i], paramList.get(i));
			} 
			catch (IOException e)
			{
				System.out.println("Bad ID: " + idArray[i]);
				e.printStackTrace();
			}
			itemOut[i] = item;
		}
		return itemOut;
	}
	
	public String getItemEndpoint()
	{
		return ITEM_ENDPOINT;
	}
	
	public ObjectMapper getObjectMapper()
	{
		return this.objectMapper;
	}
	
	protected String buildIdParams(String[] idArray)
	{
		String out ="ids=";
		for(String id : idArray)
		{
			out = out + id + ",";
		}
		out = out.substring(0, out.length()-1);
		return out;
	}
	
	
}
