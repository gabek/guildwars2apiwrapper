package org.guild.wars.rest.items;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;

import org.guild.wars.objects.items.Item;
import org.guild.wars.objects.items.Weapon;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.InfixInfusion;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.weapons.DamageType;
import org.guild.wars.objects.items.weapons.WeaponType;
import org.guild.wars.rest.common.HTTPManager;
import org.guild.wars.rest.items.ItemApiCaller;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ItemApiCallerTest 
{
	private ItemApiCaller apiCaller;
	
	public ItemApiCallerTest()
	{
		this.apiCaller = new ItemApiCaller(new HTTPManager(), "test", new ItemMapper());
	}
	
	@Test
	public void testSecurityApiKey()
	{
		String test = apiCaller.getApiKey().replace("t","");
				
		
		String result = apiCaller.getApiKey();
		String expected = "test";
		assertEquals(expected, result);
	}
	
	@Test
	public void testIdParamBuilder()
	{
		String[] ids = {"12452", "334", "4500"};
		String result = apiCaller.buildIdParams(ids);
		
		assertEquals("ids=12452,334,4500",result);
	}
	
	@Test
	public void testGetItem()
	{
		Item result = null;
		try
		{
			 result = apiCaller.getItem("30687");
		}
		catch(IOException e)
		{
			System.out.println("Bad Item ID");
			System.out.println(e);
			assertEquals(1,2);
		}
		assertNotNull(result);
		assertEquals(ItemType.Weapon, result.getType());
		assertEquals("Incinerator", result.getName());
		assertEquals(80, result.getLevel());
		assertEquals(Rarity.Legendary, result.getRarity());
		assertEquals(100000, result.getVendorValue());
		assertEquals(4682, result.getDefaultSkin());
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		Flags[] flags = {Flags.HideSuffix, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse, Flags.DeleteWarning};
		Restrictions[] restrictions = {};
		assertEquals(30687, result.getId());
		assertEquals("[&AgHfdwAA]", result.getChatLink());
		assertEquals("https://render.guildwars2.com/file/D9B12A1EBEDC04047295CF0A66E43E650297D429/456012.png", result.getIcon());
		
		assertEquals(gameModes.length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(gameModes[i], result.getGameTypes()[i]);
		}
		assertEquals(flags.length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(flags[i], result.getFlags()[i]);
		}	
		assertEquals(restrictions.length, result.getRestrictions().length);
		assertEquals(Weapon.class, result.getDetails().getClass());
		
		Weapon resultWeapon = (Weapon) result.getDetails();
		
		WeaponType expectedWeapon = WeaponType.Dagger;
		DamageType expectedDamageType = DamageType.Fire;
		int expectedMin = 970;
		int expectedMax = 1030;
		int expectedDef = 0;
		int expectedInfusionSize = 1;
		InfixInfusion expectedFlag = InfixInfusion.Infusion;
		int expectedSuffixId = 24548;
		String expectedSecondary = "";
		int[] expectedStatChoices = {161,155,159,157,158,160,153,605,700,616,154,156,162,686,
				  559,754,753,799,1026,1067,628,1032,1111,1109,1123,1140,1085,1153,1118,1131,1222,1344,
				  1363,1364,1559,1556 };
		
		assertEquals(expectedWeapon, resultWeapon.getType());
		assertEquals(expectedDamageType, resultWeapon.getDamageType());
		assertEquals(expectedMin, resultWeapon.getMinPower());
		assertEquals(expectedMax, resultWeapon.getMaxPower());
		assertEquals(expectedDef, resultWeapon.getDefense());
		assertEquals(expectedSuffixId, resultWeapon.getSuffixItemId());
		assertEquals(expectedSecondary, resultWeapon.getSecondarySuffixItemId());
		assertEquals(expectedInfusionSize, resultWeapon.getInfusionSlots().length);
		assertEquals(expectedFlag, resultWeapon.getInfusionSlots()[0].getFlags()[0]);
		assertEquals(expectedStatChoices.length, resultWeapon.getStatChoices().length);
		for(int i = 0; i < expectedStatChoices.length; i++)
			assertEquals(expectedStatChoices[i], resultWeapon.getStatChoices()[i]);
	}
	
	@Test
	public void testGetItemList()
	{
		String[] params = {"123", "554", "2311"};
		try
		{
			Item[] result = apiCaller.getItem(params);
			assertEquals(3, result.length);
			assertEquals("Zho's Mask", result[0].getName());
			assertEquals(123, result[0].getId());
			assertEquals(ItemType.Armor, result[0].getType());
			assertEquals("Malign Studded Boots", result[1].getName());
			assertEquals(554, result[1].getId());
			assertEquals(ItemType.Armor, result[1].getType());
			assertEquals("Rampager's Cabalist Gloves of Divinity", result[2].getName());
			assertEquals(2311, result[2].getId());
			assertEquals(ItemType.Armor, result[2].getType());
		}
		catch(IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
	}
	
	
	/**
	 * This Test makes sure that when a nonexistant item is called, an exception is thrown.
	 */
	
	@Test
	public void testGetNonexistantItem()
	{
		Item result = null;
		
		try
		{
			result = apiCaller.getItem("5555555555");
		}
		catch(IOException e)
		{
			return;
		}
		assertNotNull(result);
	}
	
	@Test
	public void testGetNonexistantItemInArray()
	{		
		String[] params = {"123", "554", "2311", "5555555555"};
		try
		{
			Item[] result = apiCaller.getItem(params);
			assertEquals(3,result.length);
		}
		catch(IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
	}

}
