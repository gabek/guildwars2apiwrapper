package org.guild.wars.rest.finishers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.guild.wars.objects.finishers.Finisher;
import org.guild.wars.rest.common.GuildWars2ApiCaller;
import org.guild.wars.rest.common.HTTPManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Gab
 * TODO: Investigate the parameters page and page size.
 */

public class FinisherApiCaller extends GuildWars2ApiCaller
{
	
	private final static String FINISH_ENDPOINT = "https://api.guildwars2.com/v2/finishers";
	private ObjectMapper objectMapper;

	public FinisherApiCaller(HTTPManager httpManager, String apiKey, ObjectMapper objectMapper)
	{
		super(httpManager, apiKey);
		this.objectMapper = objectMapper;
	}
	
	public Finisher getFinisher(String id) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher finisher = null;
		String url = FINISH_ENDPOINT + "/" +  id;
		String jsonFinish = this.getHTTPManager().getEndpoint(url);
		
		finisher = objectMapper.readValue(jsonFinish, Finisher.class);
		

		if(finisher == null || finisher.getName() == null)
			throw new IOException();
		
		return finisher;
	}
	
	public Finisher getFinisher(String id, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher finisher = null;
		String url = FINISH_ENDPOINT + "/" +  id;
		String jsonFinish = this.getHTTPManager().getEndpoint(url, params);

		finisher = objectMapper.readValue(jsonFinish, Finisher.class);
		if(finisher == null || finisher.getName() == null)
			throw new IOException();
		
		return finisher;
	}
	
	public Finisher[] getFinishers(String... idArray) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		String ids = buildIdParams(idArray);
		String[] params = {ids};
		String jsonFinishers = this.getHTTPManager().getEndpoint(FINISH_ENDPOINT, params);

		finisherArray = objectMapper.readValue(jsonFinishers, Finisher[].class);
		
		return finisherArray;
	}
	
	public Finisher[] getFinishers(String[] idArray, String[] params) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		String ids = buildIdParams(idArray);
		String[] paramsUnion = ArrayUtils.addAll(params, ids);
		String jsonFinishers = this.getHTTPManager().getEndpoint(FINISH_ENDPOINT, paramsUnion);

		finisherArray = objectMapper.readValue(jsonFinishers, Finisher[].class);

		return finisherArray;
	}
	
	public Finisher[] getFinishers(String[] idArray, List<String[]> paramList)
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		for(int i = 0; i< idArray.length; i++)
		{
			Finisher finish = new Finisher();
			try
			{
				finish = this.getFinisher(idArray[i], paramList.get(i));
			} 
			catch (IOException e)
			{
				System.out.println("Bad finisher id: " + idArray[i]);
				e.printStackTrace();
			}
			finisherArray[i] = finish;
		}
		return finisherArray;
	}
	
	public Finisher getAuthFinisher(String id) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher finisher = null;
		String url = FINISH_ENDPOINT + "/" +  id;
		String jsonFinish = this.getHTTPManager().getEndpoint(url, this.getApiKey());
		
		finisher = objectMapper.readValue(jsonFinish, Finisher.class);


		if(finisher == null || finisher.getName() == null)
			throw new IOException();
		
		return finisher;
	}
	
	public Finisher getAuthFinisher(String id, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher finisher = null;
		String url = FINISH_ENDPOINT + "/" +  id;
		String jsonFinish = this.getHTTPManager().getEndpoint(url, this.getApiKey(), params);
		
		finisher = objectMapper.readValue(jsonFinish, Finisher.class);

		if(finisher == null || finisher.getName() == null)
			throw new IOException();
		
		return finisher;
	}
	
	public Finisher[] getAuthFinishers(String... idArray) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		String ids = buildIdParams(idArray);
		String[] params = {ids};
		String jsonFinishers = this.getHTTPManager().getEndpoint(FINISH_ENDPOINT, this.getApiKey(), params);

		finisherArray = objectMapper.readValue(jsonFinishers, Finisher[].class);
	
		return finisherArray;
	}
	
	public Finisher[] getAuthFinishers(String[] idArray, String[] params) throws JsonParseException, JsonMappingException, IOException
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		String ids = buildIdParams(idArray);
		String[] paramsUnion = ArrayUtils.addAll(params, ids);
		String jsonFinishers = this.getHTTPManager().getEndpoint(FINISH_ENDPOINT, this.getApiKey(), paramsUnion);

		finisherArray = objectMapper.readValue(jsonFinishers, Finisher[].class);
		

		return finisherArray;
	}
	
	public Finisher[] getAuthFinishers(String[] idArray, List<String[]> paramList)
	{
		Finisher[] finisherArray = new Finisher[idArray.length];
		for(int i = 0; i< idArray.length; i++)
		{
			Finisher finish = new Finisher();
			try
			{
				finish = this.getAuthFinisher(idArray[i], paramList.get(i));
			} 
			catch (IOException e)
			{
				System.out.println("Bad finisher id: " + idArray[i]);
				e.printStackTrace();
			}
			finisherArray[i] = finish;
		}
		return finisherArray;
	}
	
	public String[] getFinisherIds() throws JsonParseException, JsonMappingException, IOException
	{
		String finisherList = this.getHTTPManager().getEndpoint(FINISH_ENDPOINT);
		return objectMapper.readValue(finisherList, String[].class);
	}
	
	protected String buildIdParams(String[] idArray)
	{
		String out ="ids=";
		for(String id : idArray)
		{
			out = out + id + ",";
		}
		out = out.substring(0, out.length()-1);
		return out;
	}

}
