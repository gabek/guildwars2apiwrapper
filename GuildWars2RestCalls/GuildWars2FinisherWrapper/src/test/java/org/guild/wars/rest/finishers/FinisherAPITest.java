package org.guild.wars.rest.finishers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;

import org.guild.wars.objects.finishers.Finisher;
import org.guild.wars.rest.common.HTTPManager;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FinisherAPITest
{
	private FinisherApiCaller caller;
	
	public FinisherAPITest()
	{
		this.caller = new FinisherApiCaller(new HTTPManager(), "test", new ObjectMapper());
	}
	
	@Test
	public void testGetFinisher()
	{
		
		try
		{
			Finisher result = caller.getFinisher("45");
			int expectedId = 45;
			String expectedDetails = "<c=@reminder>Unlock this finisher by discovering it in Black Lion Chests.</c>";
			int[] expectedItems = {64027};
			int expectedOrder = 70;
			String expectedIcon = "https://render.guildwars2.com/file/F4AC42A039CE79E3D060292E4313AB29F074CABD/867424.png";
			String expectedName = "Avatar of Death Finisher";
			assertEquals(expectedId, result.getId());
			assertEquals(expectedDetails, result.getUnlockDetails());
			assertEquals(expectedItems.length, result.getUnlockItems().length);
			for(int i = 0; i <expectedItems.length; i++)
				assertEquals(expectedItems[i], result.getUnlockItems()[i]);
			assertEquals(expectedOrder, result.getOrder());
			assertEquals(expectedIcon, result.getIcon());
			assertEquals(expectedName, result.getName());
		} 
		catch (IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
		
	}
	
	@Test
	public void testGetNonexistantFinisher()
	{
		Finisher result = null;
		
		try
		{
			result = caller.getFinisher("5555555555");
		}
		catch(IOException e)
		{
			return;
		}
		assertEquals(1,2);
	}
	
	/**
	 * Format is due to the fact that I expect the list of finishers to change.
	 * So best to test if its greater than an expected number.
	 */
	@Test
	public void testGetFinisherIds()
	{
		try
		{
			String[] list = caller.getFinisherIds();
			assertNotNull(list);
			boolean result = list.length >= 69;
			assertEquals(true, result);
		} 
		catch (JsonParseException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		} 
		catch (JsonMappingException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		} 
		catch (IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
	}
	
	@Test
	public void testGetFinisherWithParams()
	{
		String[] params = {"lang=eng", "page=0", "page_size=1"};
		try
		{
			Finisher result = caller.getFinisher("2", params);
			
			int expectedId = 2;
			String expectedDetails = "<c=@reminder>Unlock this PvP rank finisher by earning rank points and increasing your PvP rank.</c>";
			int[] expectedItems = {};
			int expectedOrder = 17;
			String expectedIcon = "https://render.guildwars2.com/file/6A68492F14AB3AFC5A95BCE008BDE8B50EB82CAF/620104.png";
			String expectedName = "Deer Rank Finisher";
			assertEquals(expectedId, result.getId());
			assertEquals(expectedDetails, result.getUnlockDetails());
			assertEquals(expectedItems.length, result.getUnlockItems().length);
			for(int i = 0; i <expectedItems.length; i++)
				assertEquals(expectedItems[i], result.getUnlockItems()[i]);
			assertEquals(expectedOrder, result.getOrder());
			assertEquals(expectedIcon, result.getIcon());
			assertEquals(expectedName, result.getName());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
	}
	
	@Test
	public void testGetFinisherList()
	{
		String[] ids = {"2","4","1"};
		try
		{
			Finisher[] result = caller.getFinishers(ids);
		
			assertEquals(3, result.length);
			assertEquals(2, result[0].getId());
			assertEquals("Deer Rank Finisher", result[0].getName());
			assertEquals(4, result[1].getId());
			assertEquals("Wolf Rank Finisher", result[1].getName());
			assertEquals(1, result[2].getId());
			assertEquals("Rabbit Rank Finisher", result[2].getName());
		}
		catch(IOException e)
		{
			e.printStackTrace();
			assertEquals(1,2);
		}
		
	}
}
