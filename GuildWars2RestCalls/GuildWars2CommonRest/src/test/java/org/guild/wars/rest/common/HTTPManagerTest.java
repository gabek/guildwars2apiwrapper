package org.guild.wars.rest.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class HTTPManagerTest 
{
	private HTTPManager manager;
	
	public HTTPManagerTest()
	{
		this.manager = new HTTPManager();
	}
	
	@Test
	public void testBuildParameters()
	{
		String[] params = {"ids=12452", "lang=dest"};
		String actual = manager.buildParameters(params);
		
		String expected = "?ids=12452&lang=dest";
		assertEquals(expected, actual);
	}
}
