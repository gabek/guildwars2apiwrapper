package org.guild.wars.rest.common;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;

/**
 * 
 * @author Gab
 * Marked as deprecated as I don't think I'll need it.
 * Guild Wars API probably doesn't allow me to delete items but hey
 * you never know.
 */

@Deprecated
public class APIDelete 
{
private HttpClient client;
	
	protected APIDelete(HttpClient client)
	{
		this.client = client;
	}
	
	protected HttpResponse deleteEndpoint(String url)
	{
		HttpDelete request = new HttpDelete(url);
		HttpResponse response = null;
		try
		{
			response = client.execute(request);
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to delete at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to delete at " + url + " due to: " + e;
		}
		return response;
	}
}
