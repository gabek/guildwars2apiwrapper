package org.guild.wars.rest.common;

public abstract class GuildWars2ApiCaller
{
	private HTTPManager httpManager;
	
	private String apiKey;
	
	public GuildWars2ApiCaller(HTTPManager httpManager, String apiKey)
	{
		this.httpManager = httpManager;
		this.apiKey = apiKey;
	}
	
	public HTTPManager getHTTPManager()
	{
		return this.httpManager;
	}
	
	public String getApiKey()
	{
		return this.apiKey;
	}
}
