package org.guild.wars.rest.common;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

/**
 * 
 * @author Gab
 * TODO: Get a nice logger for this to be honest.
 * Logger are so damn useful.
 */
public class APIGet
{
	private HttpClient client;
	
	protected APIGet(HttpClient client)
	{
		this.client = client;
	}
	
	/**
	 * Makes a HTTP get to an endpoint.
	 * @param url The url of the endpoint.
	 * @return A {@link HttpResponse} of the endpoint. Expected to contain a JSON string.
	 */
	protected HttpResponse getEndpoint(String url)
	{
		HttpGet request = new HttpGet(url);
		HttpResponse response = null;
		try 
		{
			response = client.execute(request);
			
		} 
		catch (ClientProtocolException e) 
		{
			String error = "Failed to get " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to get " + url + " due to: " + e;
		}
		return response;
	}
	
	/**
	 * A call to an get endpoint with an API key.
	 * In practice nothing more than a wrapper for {@link #getEndpoint(String) getEndpoint}
	 * @param url the url of the endpoint.
	 * @param apiKey the apikey of the endpoint.
	 * @return {@link HttpResponse} of the endpoint. Expected to contain a JSON string.
	 */
	
	protected HttpResponse getAuthorizedEndpoint(String url, String apiKey)
	{
		return getEndpoint(url + "?access_token=" + apiKey);
	}
	
}
