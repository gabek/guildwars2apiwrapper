package org.guild.wars.rest.common;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 * 
 * @author Gab
 * May change to a better name.
 * TODO: Make parameters better.
 */
public class HTTPManager
{
	private HttpClient client;
	
	private APIGet getHandler;
	
	private APIPost postHandler;
	
	private APIPut putHandler;
	
	private APIDelete deleteHandler;
	
	public HTTPManager()
	{
		HttpClientBuilder builder = HttpClientBuilder.create();
		this.client = builder.build();
		
		this.getHandler = new APIGet(client);
		this.postHandler = new APIPost(client);
		this.putHandler = new APIPut(client);
		this.deleteHandler = new APIDelete(client);
	}
	
	public HTTPManager(HttpClient client)
	{
		this.client = client;
		
		this.getHandler = new APIGet(client);
		this.postHandler = new APIPost(client);
		this.putHandler = new APIPut(client);
		this.deleteHandler = new APIDelete(client);
	}
	
	public String getEndpoint(String url)
	{
		HttpResponse response = this.getHandler.getEndpoint(url);
		return responseToJson(response);
	}
	
	
	public String getEndpoint(String url, String apiKey)
	{
		HttpResponse response = this.getHandler.getAuthorizedEndpoint(url, apiKey);
		return responseToJson(response);
	}
	
	public String getEndpoint(String url, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.getHandler.getEndpoint(url);
		return responseToJson(response);
	}
	
	public String getEndpoint(String url, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.getHandler.getAuthorizedEndpoint(url, apiKey);
		return responseToJson(response);
	}
	
	public HttpResponse getEndpointResponse(String url)
	{
		HttpResponse response = this.getHandler.getEndpoint(url);
		return response;
	}
	
	public HttpResponse getEndpointResponse(String url, String apiKey)
	{
		HttpResponse response = this.getHandler.getAuthorizedEndpoint(url, apiKey);
		return response;
	}
	
	public HttpResponse getEndpointResponse(String url, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.getHandler.getEndpoint(url);
		return response;
	}
	
	public HttpResponse getEndpointResponse(String url, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.getHandler.getAuthorizedEndpoint(url, apiKey);
		return response;
	}
	
	public String postEndpoint(String url)
	{
		HttpResponse response = this.postHandler.postEndpoint(url);
		return responseToJson(response);
	}
	
	public String postEndpoint(String url, String body)
	{
		HttpResponse response = this.postHandler.postEndpoint(url, body);
		return responseToJson(response);
	}
	
	public String postEndpoint(String url, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postEndpoint(url);
		return responseToJson(response);
	}
	
	public String postEndpoint(String url, String body, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postEndpoint(url, body);
		return responseToJson(response);
	}
	
	public String postAuthEndpoint(String url, String apiKey)
	{
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, apiKey);
		return responseToJson(response);
	}
	
	public String postAuthEndpoint(String url, String body, String apiKey)
	{
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, body, apiKey);
		return responseToJson(response);
	}
	
	public String postAuthEndpoint(String url, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, apiKey);
		return responseToJson(response);
	}
	
	public String postAuthEndpoint(String url, String body, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, body, apiKey);
		return responseToJson(response);
	}
	
	public HttpResponse postEndpointResponse(String url)
	{
		HttpResponse response = this.postHandler.postEndpoint(url);
		return response;
	}
	
	public HttpResponse postEndpointResponse(String url, String body)
	{
		HttpResponse response = this.postHandler.postEndpoint(url, body);
		return response;
	}
	
	public HttpResponse postEndpointResponse(String url, String body, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postEndpoint(url, body);
		return response;
	}
	
	public HttpResponse postAuthEndpointResponse(String url, String apiKey)
	{
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, apiKey);
		return response;
	}
	
	public HttpResponse postAuthEndpointResponse(String url, String body, String apiKey)
	{
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, body, apiKey);
		return response;
	}
	
	public HttpResponse postAuthEndpointResponse(String url, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, apiKey);
		return response;
	}
	
	public HttpResponse postAuthEndpointResponse(String url, String body, String apiKey, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.postHandler.postAuthorizedEndpoint(url, body, apiKey);
		return response;
	}
	
	@Deprecated
	public HttpResponse putEndpoint(String url)
	{
		HttpResponse response = this.putHandler.putEndpoint(url);
		return response;
	}
	
	@Deprecated
	public HttpResponse putEndpoint(String url, String body)
	{
		HttpResponse response = this.putHandler.putEndpoint(url, body);
		return response;
	}
	
	@Deprecated
	public HttpResponse putEndpoint(String url, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.putHandler.putEndpoint(url);
		return response;
	}
	
	@Deprecated
	public HttpResponse putEndpoint(String url, String body, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.putHandler.putEndpoint(url, body);
		return response;
	}
	
	@Deprecated
	public HttpResponse deleteEndpoint(String url)
	{
		HttpResponse response = this.deleteHandler.deleteEndpoint(url);
		return response;
	}
	
	@Deprecated
	public HttpResponse deleteEndpoint(String url, String... params)
	{
		url = url + buildParameters(params);
		HttpResponse response = this.deleteHandler.deleteEndpoint(url);
		return response;
	}
	
	protected String responseToJson(HttpResponse response)
	{
		String output = null;
		try 
		{
			System.out.println(response);
			System.out.println(response.getEntity());
			output = EntityUtils.toString(response.getEntity());
		}
		catch (ParseException e)
		{
			
			String error = "Failed to parse json: " + response.toString() + " due to: " + e;
			System.out.println(error);
		}
		catch (IOException e)
		{
			String error = "Unknown IOException " + response.toString() + " due to: " + e;
			System.out.println(error);
		}
		return output;
	}
	
	protected String buildParameters(String... params)
	{
		String out = "?";
		for(int i = 0; i < params.length; i++)
		{
			out = out + params[i] + "&";
		}
		out = out.substring(0, out.length()-1);
		return out;
	}
}
