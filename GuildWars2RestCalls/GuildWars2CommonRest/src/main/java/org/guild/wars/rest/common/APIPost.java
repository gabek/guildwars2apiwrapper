package org.guild.wars.rest.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

public class APIPost 
{
	private HttpClient client;
	
	protected APIPost(HttpClient client)
	{
		this.client = client;
	}
	
	/**
	 * Makes a HTTP post to an endpoint
	 * @param url The url of the endpoint.
	 * @return A {@link HttpResponse} of the endpoint.
	 */
	protected HttpResponse postEndpoint(String url)
	{
		HttpPost request = new HttpPost(url);
		HttpResponse response = null;
		try
		{
			response = client.execute(request);
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to post at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to post at " + url + " due to: " + e;
		}
		return response;
	}
	
	/**
	 * A post to an endpoint with a body.
	 * @param url The url of the endpoint.
	 * @param body The body of the post.
	 * @return A {@link HttpResponse} of the endpoint.
	 */
	protected HttpResponse postEndpoint(String url, String body)
	{
		HttpPost request = new HttpPost(url);
		HttpResponse response = null;
		try
		{
			request.setEntity(new StringEntity(body));
		
			response = client.execute(request);
		}
		catch(UnsupportedEncodingException e)
		{
			String error = "Failed to encode body: " + body + " due to: " + e;
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to post " + body + " at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to post " + body + " at " + url + " due to: " + e;
		}
		return response;
	}
	
	/**
	 * A post to an endpoint that requires the API Key. 
	 * @param url The url of the endpoint
	 * @param body The body of the post.
	 * @param apiKey 
	 * @return {@link HttpResponse}
	 */
	protected HttpResponse postAuthorizedEndpoint(String url, String body, String apiKey)
	{
		HttpPost request = new HttpPost(url);
		HttpResponse response = null;
		try
		{
			request.setEntity(new StringEntity(body));
			request.setHeader(" Authorization", "Bearer " + apiKey);
			response = client.execute(request);
		}
		catch(UnsupportedEncodingException e)
		{
			String error = "Failed to encode body: " + body + " due to: " + e;
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to post " + body + " at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to post " + body + " at " + url + " due to: " + e;
		}
		return response;
	}
	
	protected HttpResponse postAuthorizedEndpoint(String url, String apiKey)
	{
		HttpPost request = new HttpPost(url);
		HttpResponse response = null;
		try
		{
			request.setHeader(" Authorization", "Bearer " + apiKey);
			response = client.execute(request);
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to post at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to post at " + url + " due to: " + e;
		}
		return response;
	}
}
