package org.guild.wars.rest.common;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;

/**
 * 
 * @author Gab
 * May not be used will mark as depecrated until i know I don't need this.
 */

@Deprecated
public class APIPut {
	
	private HttpClient client;
	
	protected APIPut(HttpClient client)
	{
		this.client = client;
	}
	
	protected HttpResponse putEndpoint(String url)
	{
		HttpPut request = new HttpPut(url);
		HttpResponse response = null;
		try
		{
			response = client.execute(request);
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to put at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to put at " + url + " due to: " + e;
		}
		return response;
	}
	
	protected HttpResponse putEndpoint(String url, String body)
	{
		HttpPut request = new HttpPut(url);
		HttpResponse response = null;
		try
		{
			request.setEntity(new StringEntity(body));
			response = client.execute(request);
		}
		catch(UnsupportedEncodingException e)
		{
			String error = "Failed to encode body: " + body + " due to: " + e;
		}
		catch(ClientProtocolException e)
		{
			String error = "Failed to put at " + url + " due to: " + e;
		} 
		catch (IOException e)
		{
			String error = "Failed to put at " + url + " due to: " + e;
		}
		return response;
	}
}
