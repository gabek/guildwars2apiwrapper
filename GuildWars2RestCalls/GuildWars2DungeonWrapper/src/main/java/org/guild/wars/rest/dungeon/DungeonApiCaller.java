package org.guild.wars.rest.dungeon;

import java.io.IOException;

import org.guild.wars.objects.dungeons.Dungeon;
import org.guild.wars.rest.common.GuildWars2ApiCaller;
import org.guild.wars.rest.common.HTTPManager;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DungeonApiCaller extends GuildWars2ApiCaller
{
	
	private final static String DUNGEON_ENDPOINT ="https://api.guildwars2.com/v2/dungeons";
	private ObjectMapper objectMapper;
	
	public DungeonApiCaller(HTTPManager httpManager, String apiKey, ObjectMapper objectMapper)
	{
		super(httpManager, apiKey);
		this.objectMapper = objectMapper;
	}
	
	public Dungeon getDungeon(String dungeonName) throws JsonParseException, JsonMappingException, IOException
	{
		Dungeon dungeon = null;
		String url = DUNGEON_ENDPOINT + "/" + dungeonName;
		String jsonDungeon = this.getHTTPManager().getEndpoint(url);

		dungeon = objectMapper.readValue(jsonDungeon, Dungeon.class);

		if(dungeon == null || dungeon.getId() == null)
			throw new IOException();
		return dungeon;
	}
	
	public Dungeon getDungeon(String dungeonName, String... params) throws JsonParseException, JsonMappingException, IOException
	{
		Dungeon dungeon = null;
		String url = DUNGEON_ENDPOINT + "/" + dungeonName;
		String jsonDungeon = this.getHTTPManager().getEndpoint(url, params);
		
		dungeon = objectMapper.readValue(jsonDungeon, Dungeon.class);
		
		if(dungeon == null || dungeon.getId()==null)
			throw new IOException();
		return dungeon;
	}
	
	public String[] getDungeonNames() throws JsonParseException, JsonMappingException, IOException
	{
		String dungeonList = this.getHTTPManager().getEndpoint(DUNGEON_ENDPOINT);
		return objectMapper.readValue(dungeonList, String[].class);
	}
}
