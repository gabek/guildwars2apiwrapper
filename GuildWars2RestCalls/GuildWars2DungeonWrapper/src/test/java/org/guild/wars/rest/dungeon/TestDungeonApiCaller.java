package org.guild.wars.rest.dungeon;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.IOException;

import org.guild.wars.objects.dungeons.Dungeon;
import org.guild.wars.objects.dungeons.DungeonId;
import org.guild.wars.objects.dungeons.DungeonType;
import org.guild.wars.rest.common.HTTPManager;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestDungeonApiCaller
{
	private DungeonApiCaller caller;
	
	public TestDungeonApiCaller()
	{
		this.caller = new DungeonApiCaller(new HTTPManager(), "test", new ObjectMapper());
	}
	
	@Test
	public void testGetDungeon()
	{
		Dungeon result;
		try
		{
			result = caller.getDungeon("citadel_of_flame");
			String[] expectedName = {"cof_story", "ferrah", "magg", "rhiannon"};
			DungeonType[] expectedType = {DungeonType.Story, DungeonType.Explorable, DungeonType.Explorable,
					DungeonType.Explorable};
			
			assertEquals(DungeonId.CitadelOfFlame, result.getId());
			assertEquals(4, result.getPaths().length);
			
			for(int i = 0; i < result.getPaths().length; i++)
			{
				assertEquals(expectedName[i], result.getPaths()[i].getId());
				assertEquals(expectedType[i], result.getPaths()[i].getType());
			}
		} 
		catch (IOException e)
		{
			assertEquals(1,2);
		}	
	}
	
	@Test
	public void testGetDungeonWithParameter()
	{
		try
		{
			String[] params = {"lang=eng"};
			Dungeon result = caller.getDungeon("ruined_city_of_arah",params);
			String[] expectedName = {"arah_story", "jotun", "mursaat", "forgotten", "seer"};
			DungeonType[] expectedType = {DungeonType.Story, DungeonType.Explorable, DungeonType.Explorable,
					DungeonType.Explorable, DungeonType.Explorable};
			
			assertEquals(DungeonId.RuinedCityOfArah, result.getId());
			assertEquals(5, result.getPaths().length);
			
			for(int i = 0; i < result.getPaths().length; i++)
			{
				assertEquals(expectedName[i], result.getPaths()[i].getId());
				assertEquals(expectedType[i], result.getPaths()[i].getType());
			}
		} 
		catch (IOException e)
		{
			assertEquals(1,2);
		}	
	}
	
	@Test
	public void testGetDungeonNames()
	{
		try
		{
			String[] result = caller.getDungeonNames();
			String[] expected = {"ascalonian_catacombs", "caudecus_manor", "twilight_arbor", "sorrows_embrace", "citadel_of_flame",
						"honor_of_the_waves", "crucible_of_eternity", "ruined_city_of_arah"};
			assertEquals(expected.length, result.length);
			for(int i = 0; i< expected.length; i++)
			{
				assertEquals(expected[i], result[i]);
			}
		}
		catch(IOException e)
		{
			assertEquals(1,2);
		}
	}
	
	@Test
	public void testGetBadDungeon()
	{
		Dungeon result = null;
		try
		{
			result = caller.getDungeon("adfasfasdf");
			assertEquals(1,2);
			
		} 
		catch (IOException e)
		{
			assertNull(result);
		}	
	}
	
}
