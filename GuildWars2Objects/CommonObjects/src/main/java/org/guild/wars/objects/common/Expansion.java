package org.guild.wars.objects.common;

public enum Expansion
{
	Base ("Base"),
	HeartOfThorns ("Heart of Thorns"),
	PathOfFire ("Path of Fire");
	
	private final String value;
	
	Expansion(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}

}
