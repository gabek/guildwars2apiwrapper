package org.guild.wars.objects.finishers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Finisher
{
	@JsonProperty(value="id", required=true)
	private int id;
	
	@JsonProperty(value="unlock_details", required=true)
	private String unlockDetails;
	
	@JsonProperty(value="unlock_items")
	private int[] unlockItems;
	
	@JsonProperty(value="order", required=true)
	private int order;
	
	@JsonProperty(value="icon", required=true)
	private String icon;
	
	@JsonProperty(value="name", required=true)
	private String name;

	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the unlockDetails
	 */
	public String getUnlockDetails()
	{
		return unlockDetails;
	}

	/**
	 * @return the unlockItems
	 */
	public int[] getUnlockItems()
	{
		return unlockItems;
	}

	/**
	 * @return the order
	 */
	public int getOrder()
	{
		return order;
	}

	/**
	 * @return the icon
	 */
	public String getIcon()
	{
		return icon;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	
	
}
