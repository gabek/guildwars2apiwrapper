package org.guild.wars.objects.finishers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class FinisherTest
{
	private ObjectMapper mapper;
	
	public FinisherTest()
	{
		this.mapper = new ObjectMapper();
	}
	
	@Test
	public void testFinisher1() throws IOException
	{
		InputStream stream = getClass().getClassLoader().getResourceAsStream("test1.json");
		String finisher = IOUtils.toString(stream, "utf-8");
		
		Finisher result = mapper.readValue(finisher, Finisher.class);
		
		int expectedId = 56;
		String expectedDetails = "<c=@reminder>Rewarded for finishing fourth in the World Tournament Series!</c>";
		int[] expectedItems = {68105};
		int expectedOrder = 3;
		String expectedIcon = "https://render.guildwars2.com/file/A0A0CA0821CA3A2A1BE4C09C463514FC0FB1201D/924749.png";
		String expectedName = "4th Place World Tournament Series Finisher";
		assertEquals(expectedId, result.getId());
		assertEquals(expectedDetails, result.getUnlockDetails());
		assertEquals(expectedItems.length, result.getUnlockItems().length);
		for(int i = 0; i <expectedItems.length; i++)
			assertEquals(expectedItems[i], result.getUnlockItems()[i]);
		assertEquals(expectedOrder, result.getOrder());
		assertEquals(expectedIcon, result.getIcon());
		assertEquals(expectedName, result.getName());
		
	}
	
	@Test
	public void testFinisher2() throws IOException
	{
		InputStream stream = getClass().getClassLoader().getResourceAsStream("test2.json");
		String finisher = IOUtils.toString(stream, "utf-8");
		
		Finisher result = mapper.readValue(finisher, Finisher.class);
		
		int expectedId = 1;
		String expectedDetails = "<c=@reminder>Unlock this PvP rank finisher by earning rank points and increasing your PvP rank.</c>";
		int[] expectedItems = {};
		int expectedOrder = 18;
		String expectedIcon = "https://render.guildwars2.com/file/807516C20D08B908946167EADD57980163EECA4E/620101.png";
		String expectedName = "Rabbit Rank Finisher";
		assertEquals(expectedId, result.getId());
		assertEquals(expectedDetails, result.getUnlockDetails());
		assertEquals(expectedItems.length, result.getUnlockItems().length);
		for(int i = 0; i <expectedItems.length; i++)
			assertEquals(expectedItems[i], result.getUnlockItems()[i]);
		assertEquals(expectedOrder, result.getOrder());
		assertEquals(expectedIcon, result.getIcon());
		assertEquals(expectedName, result.getName());
		
	}
	
	@Test
	public void testFinisher3() throws IOException
	{
		InputStream stream = getClass().getClassLoader().getResourceAsStream("test3.json");
		String finisher = IOUtils.toString(stream, "utf-8");
		
		Finisher result = mapper.readValue(finisher, Finisher.class);
		
		int expectedId = 45;
		String expectedDetails = "<c=@reminder>Unlock this finisher by discovering it in Black Lion Chests.</c>";
		int[] expectedItems = {64027};
		int expectedOrder = 70;
		String expectedIcon = "https://render.guildwars2.com/file/F4AC42A039CE79E3D060292E4313AB29F074CABD/867424.png";
		String expectedName = "Avatar of Death Finisher";
		assertEquals(expectedId, result.getId());
		assertEquals(expectedDetails, result.getUnlockDetails());
		assertEquals(expectedItems.length, result.getUnlockItems().length);
		for(int i = 0; i <expectedItems.length; i++)
			assertEquals(expectedItems[i], result.getUnlockItems()[i]);
		assertEquals(expectedOrder, result.getOrder());
		assertEquals(expectedIcon, result.getIcon());
		assertEquals(expectedName, result.getName());
		
	}
}
