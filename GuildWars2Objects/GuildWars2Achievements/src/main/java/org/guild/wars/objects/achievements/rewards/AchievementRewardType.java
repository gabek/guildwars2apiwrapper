package org.guild.wars.objects.achievements.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum AchievementRewardType
{
	@JsonProperty("Coins")
	Coins ("Coins"),
	@JsonProperty("Item")
	Item ("Item"),
	@JsonProperty("Mastery")
	Mastery ("Mastery"),
	@JsonProperty("Title")
	Title ("Title");
	
	private final String value;
	
	AchievementRewardType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
