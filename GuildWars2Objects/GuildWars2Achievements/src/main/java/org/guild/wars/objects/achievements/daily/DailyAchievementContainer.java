package org.guild.wars.objects.achievements.daily;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyAchievementContainer
{
	@JsonProperty(value="pve", required=true)
	DailyAchievement[] pve;
	
	@JsonProperty(value="pvp", required=true)
	DailyAchievement[] pvp;
	
	@JsonProperty(value="wvw", required=true)
	DailyAchievement[] wvw;
	
	@JsonProperty(value="fractals", required=true)
	DailyAchievement[] fractals;
	
	@JsonProperty(value="special", required=true)
	DailyAchievement[] special;

	/**
	 * @return the pve
	 */
	public DailyAchievement[] getPve()
	{
		return pve;
	}

	/**
	 * @return the pvp
	 */
	public DailyAchievement[] getPvp()
	{
		return pvp;
	}

	/**
	 * @return the wvw
	 */
	public DailyAchievement[] getWvw()
	{
		return wvw;
	}

	/**
	 * @return the fractals
	 */
	public DailyAchievement[] getFractals()
	{
		return fractals;
	}

	/**
	 * @return the special
	 */
	public DailyAchievement[] getSpecial()
	{
		return special;
	}
}
