package org.guild.wars.objects.achievements;

import org.guild.wars.objects.achievements.rewards.AchievementReward;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Achievement
{
	@JsonProperty(value="id", required=true)
	private String id;
	
	@JsonProperty("icon")
	private String icon;
	
	@JsonProperty(value="name", required=true)
	private String name;
	
	@JsonProperty(value="description", required=true)
	private String description;
	
	@JsonProperty(value="requirement", required=true)
	private String requirement;
	
	@JsonProperty(value="locked_text", required=true)
	private String lockedText;
	
	@JsonProperty(value="type", required=true)
	private AchievementType type;
	
	@JsonProperty(value="flags", required=true)
	private AchievementTag[] flags;
	
	@JsonProperty(value="tiers", required=true)
	private AchievementTiers[] tiers;
	
	@JsonProperty("prerequisites")
	private int[] prerequisites;
	
	@JsonProperty("rewards")
	private AchievementReward[] rewards;
	
	@JsonProperty("bits")
	private InfoBit[] bits;
	
	@JsonProperty("point_cap")
	private int pointCap;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the icon
	 */
	public String getIcon()
	{
		return icon;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the requirement
	 */
	public String getRequirement()
	{
		return requirement;
	}

	/**
	 * @return the lockedText
	 */
	public String getLockedText()
	{
		return lockedText;
	}

	/**
	 * @return the type
	 */
	public AchievementType getType()
	{
		return type;
	}

	/**
	 * @return the flags
	 */
	public AchievementTag[] getFlags()
	{
		return flags;
	}

	/**
	 * @return the tiers
	 */
	public AchievementTiers[] getTiers()
	{
		return tiers;
	}

	/**
	 * @return the prerequisites
	 */
	public int[] getPrerequisites()
	{
		return prerequisites;
	}

	/**
	 * @return the rewards
	 */
	public AchievementReward[] getRewards()
	{
		return rewards;
	}

	/**
	 * @return the bits
	 */
	public InfoBit[] getBits()
	{
		return bits;
	}

	/**
	 * @return the pointCap
	 */
	public int getPointCap()
	{
		return pointCap;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @param icon the icon to set
	 */
	protected void setIcon(String icon)
	{
		this.icon = icon;
	}

	/**
	 * @param name the name to set
	 */
	protected void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param description the description to set
	 */
	protected void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param requirement the requirement to set
	 */
	protected void setRequirement(String requirement)
	{
		this.requirement = requirement;
	}

	/**
	 * @param lockedText the lockedText to set
	 */
	protected void setLockedText(String lockedText)
	{
		this.lockedText = lockedText;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(AchievementType type)
	{
		this.type = type;
	}

	/**
	 * @param flags the flags to set
	 */
	protected void setFlags(AchievementTag[] flags)
	{
		this.flags = flags;
	}

	/**
	 * @param tiers the tiers to set
	 */
	protected void setTiers(AchievementTiers[] tiers)
	{
		this.tiers = tiers;
	}

	/**
	 * @param prerequisites the prerequisites to set
	 */
	protected void setPrerequisites(int[] prerequisites)
	{
		this.prerequisites = prerequisites;
	}

	/**
	 * @param rewards the rewards to set
	 */
	protected void setRewards(AchievementReward[] rewards)
	{
		this.rewards = rewards;
	}

	/**
	 * @param bits the bits to set
	 */
	protected void setBits(InfoBit[] bits)
	{
		this.bits = bits;
	}

	/**
	 * @param pointCap the pointCap to set
	 */
	protected void setPointCap(int pointCap)
	{
		this.pointCap = pointCap;
	}
	
}
