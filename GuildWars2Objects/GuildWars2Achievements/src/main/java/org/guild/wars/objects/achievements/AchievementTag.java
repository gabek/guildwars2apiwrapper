package org.guild.wars.objects.achievements;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * 
 * A tag for the Achievement.
 */
public enum AchievementTag
{
	@JsonProperty("Pvp")
	PvP ("PvP"),
	@JsonProperty("CategoryDisplay")
	CategoryDisplay ("CategoryDisplay"),
	@JsonProperty("MoveToTop")
	MoveToTop ("MoveToTop"),
	@JsonProperty("IgnoreNearlyComplete")
	IgnoreNearlyComplete ("IgnoreNearlyComplete"),
	@JsonProperty("Repeatable")
	Repeatable ("Repeatable"),
	@JsonProperty("Hidden")
	Hidden ("Hidden"),
	@JsonProperty("RequiresUnlock")
	RequiresUnlock ("RequiresUnlock"),
	@JsonProperty("RepairOnLogin")
	RepairOnLogin ("RepairOnLogin"),
	@JsonProperty("Daily")
	Daily ("Daily"),
	@JsonProperty("Weekly")
	Weekly ("Weekly"),
	@JsonProperty("Monthly")
	Monthly ("Monthly"),
	@JsonProperty("Permanent")
	Permanent ("Permanent");
	
	private final String value;
	
	AchievementTag(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
