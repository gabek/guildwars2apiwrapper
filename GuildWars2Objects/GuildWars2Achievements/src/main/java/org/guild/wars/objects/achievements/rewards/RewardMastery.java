package org.guild.wars.objects.achievements.rewards;

import org.guild.wars.objects.common.Expansion;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RewardMastery extends AchievementReward
{
	@JsonProperty(value="id", required=true)
	private int id;
	
	@JsonProperty(value="region", required=true)
	private Expansion region;

	/**
	 * @return the id of the mastery.
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the region of the mastery.
	 */
	public Expansion getRegion()
	{
		return region;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param region the region to set
	 */
	protected void setRegion(Expansion region)
	{
		this.region = region;
	}
	
	
}
