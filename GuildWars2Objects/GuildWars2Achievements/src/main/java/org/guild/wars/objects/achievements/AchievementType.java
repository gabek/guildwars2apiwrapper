package org.guild.wars.objects.achievements;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * An enumerator for the type of achivement.
 */

public enum AchievementType
{
	@JsonProperty("Default")
	Default ("Default"),
	@JsonProperty("ItemSet")
	ItemSet ("ItemSet");
	
	private final String value;
	
	AchievementType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}
}
