package org.guild.wars.objects.achievements.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RewardCoins extends AchievementReward
{
	@JsonProperty(value="count", required=true)
	private int count;
	
	/**
	 * 
	 * @return count. The number of coins to be awarded.
	 */
	public int getCount()
	{
		return this.count;
	}
	
	protected void setCount(int count)
	{
		this.count = count;
	}
}
