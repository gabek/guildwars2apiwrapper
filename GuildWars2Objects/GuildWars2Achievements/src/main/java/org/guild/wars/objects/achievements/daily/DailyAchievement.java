package org.guild.wars.objects.achievements.daily;

import org.guild.wars.objects.common.Expansion;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyAchievement
{
	@JsonProperty(value="id", required=true)
	private int id;
	
	@JsonProperty(value="level", required=true)
	private DailyLevel level;
	
	@JsonProperty(value="required_access", required=true)
	private Expansion[] requiredCampaign;

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the level
	 */
	public DailyLevel getLevel()
	{
		return level;
	}

	/**
	 * @return the requiredCampaign
	 */
	public Expansion[] getRequiredCampaign()
	{
		return requiredCampaign;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param level the level to set
	 */
	protected void setLevel(DailyLevel level)
	{
		this.level = level;
	}

	/**
	 * @param requiredCampaign the requiredCampaign to set
	 */
	protected void setRequiredCampaign(Expansion[] requiredCampaign)
	{
		this.requiredCampaign = requiredCampaign;
	}
	
}
