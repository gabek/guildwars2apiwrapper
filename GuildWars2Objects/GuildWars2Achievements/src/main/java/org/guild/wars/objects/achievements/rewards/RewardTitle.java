package org.guild.wars.objects.achievements.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RewardTitle extends AchievementReward
{
	@JsonProperty(value="id", required=true)
	private int id;
	
	public int getId()
	{
		return this.id;
	}
	
	protected void setId(int id)
	{
		this.id = id;
	}
}
