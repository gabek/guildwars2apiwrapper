package org.guild.wars.objects.achievements.rewards;

import java.io.IOException;

import org.guild.wars.objects.achievements.Achievement;
import org.guild.wars.objects.common.Expansion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class AchievementRewardDeserializer extends StdDeserializer<AchievementReward>
{


	private static final long serialVersionUID = 1L;

	public AchievementRewardDeserializer()
	{
		this(null);
	}
	
	protected AchievementRewardDeserializer(Class<AchievementReward> vc)
	{
		super(vc);
	}

	@Override
	public AchievementReward deserialize(JsonParser parser, DeserializationContext context)
			throws IOException, JsonProcessingException
	{
		JsonNode node = parser.getCodec().readTree(parser);		
		
		if(node.hasNonNull("type") && node.get("type").isTextual())
		{
			String typeString = node.get("type").asText();
			AchievementRewardType type = AchievementRewardType.valueOf(typeString);
			switch(type)
			{
				case Coins:
					RewardCoins coins = new RewardCoins();
					coins.setType(type);
					if(node.hasNonNull("count") && node.get("count").isInt())
					{
						int count = node.get("count").asInt();
						coins.setCount(count);
					}
					return coins;
				case Item:
					RewardItems items = new RewardItems();
					items.setType(type);
					if(node.hasNonNull("id") && node.get("id").isInt())
					{
						int id = node.get("id").asInt();
						items.setId(id);
					}
					if(node.hasNonNull("count") && node.get("count").isInt())
					{
						int count = node.get("count").asInt();
						items.setCount(count);
					}
					return items;
				case Mastery:
					RewardMastery mastery = new RewardMastery();
					mastery.setType(type);
					if(node.hasNonNull("id") && node.get("id").isInt())
					{
						int id = node.get("id").asInt();
						mastery.setId(id);
					}
					if(node.hasNonNull("region") && node.get("region").isTextual())
					{
						String region = node.get("region").asText();
						switch(region)
						{
							case "Tyria":
								mastery.setRegion(Expansion.Base);
								break;
							case "Maguuma":
								mastery.setRegion(Expansion.HeartOfThorns);
								break;
							case "Desert":
								mastery.setRegion(Expansion.PathOfFire);
								break;
							default:
								break;
						}
					}
					return mastery;
				case Title:
					RewardTitle title = new RewardTitle();
					title.setType(type);
					if(node.hasNonNull("id") && node.get("id").isInt())
					{
						int id = node.get("id").asInt();
						title.setId(id);
					}
					return title;
				default:
					throw new IOException();				
			}
		}
		throw new IOException();
	}

}
