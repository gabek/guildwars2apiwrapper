package org.guild.wars.objects.achievements.groups;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AchievementGroups
{
	@JsonProperty(value="id", required=true)
	private String id;
	
	@JsonProperty(value="name", required=true)
	private String name;
	
	@JsonProperty(value="description", required=true)
	private String description;
	
	@JsonProperty(value="order", required=true)
	private String order;
	
	@JsonProperty(value="categories", required=true)
	private int[] categories;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the order
	 */
	public String getOrder()
	{
		return order;
	}

	/**
	 * @return the categories
	 */
	public int[] getCategories()
	{
		return categories;
	}
	
	
}
