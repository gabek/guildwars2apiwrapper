package org.guild.wars.objects.achievements.daily;

import java.io.IOException;

import org.guild.wars.objects.common.Expansion;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class DailyAchievementDeserializer extends StdDeserializer<DailyAchievement>
{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 3800941367923801229L;

	public DailyAchievementDeserializer()
	{
		this(null);
	}
	
	protected DailyAchievementDeserializer(Class<DailyAchievement> vc)
	{
		super(vc);
	}

	@Override
	public DailyAchievement deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException
	{
		DailyAchievement daily = new DailyAchievement();
		JsonNode node = parser.getCodec().readTree(parser);
		ObjectMapper mapper = (ObjectMapper) parser.getCodec();
		
		if(node.hasNonNull("id") && node.get("id").isInt())
		{
			int id = node.get("id").asInt();
			daily.setId(id);
		}
		if(node.has("level") && node.get("level").isObject())
		{
			JsonNode levelNode = node.get("level");
			DailyLevel level = mapper.treeToValue(levelNode, DailyLevel.class);
			daily.setLevel(level);
		}
		if(node.has("required_access") && node.get("required_access").isArray())
		{
			String[] accessArray = mapper.treeToValue(node.get("required_access"), String[].class);
			Expansion[] expansionArray = new Expansion[accessArray.length];
			for(int i = 0; i < accessArray.length; i++)
			{
				String expansion = accessArray[i];
				switch(expansion)
				{
					case "GuildWars2":
						expansionArray[i] = Expansion.Base;
						break;
					case "HeartOfThorns":
						expansionArray[i] = Expansion.HeartOfThorns;
						break;
					case "PathOfFire":
						expansionArray[i] = Expansion.PathOfFire;
						break;
					default:
						break;
				}
			}
			daily.setRequiredCampaign(expansionArray);
		}
		return daily;
	}

}
