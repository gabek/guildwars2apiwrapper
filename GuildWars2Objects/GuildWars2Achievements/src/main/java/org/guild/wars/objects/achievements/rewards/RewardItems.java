package org.guild.wars.objects.achievements.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RewardItems extends AchievementReward
{
	@JsonProperty(value="id", required=true)
	private int id; 
	
	@JsonProperty(value="count", required=true)
	private int count;

	/**
	 * @return the item id.
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the number of items to be rewarded of type id.
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param count the count to set
	 */
	protected void setCount(int count)
	{
		this.count = count;
	}
	
	
	
	
}
