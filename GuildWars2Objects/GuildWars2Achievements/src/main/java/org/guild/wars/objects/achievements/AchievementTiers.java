package org.guild.wars.objects.achievements;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * Represents a tier in an achievement.
 *
 */

public class AchievementTiers
{
	@JsonProperty(value="count", required=true)
	private int count;
	
	@JsonProperty(value="points", required=true)
	private int points;

	/**
	 * @return the count. The number of "things" to unlock this tier.
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * @return the points. The number of AP you get as a reward for completing this tier.
	 */
	public int getPoints()
	{
		return points;
	}
	
	
}
