package org.guild.wars.objects.achievements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.guild.wars.objects.achievements.rewards.AchievementReward;
import org.guild.wars.objects.achievements.rewards.AchievementRewardDeserializer;
import org.guild.wars.objects.achievements.rewards.AchievementRewardType;
import org.guild.wars.objects.achievements.rewards.RewardCoins;
import org.guild.wars.objects.achievements.rewards.RewardItems;
import org.guild.wars.objects.achievements.rewards.RewardMastery;
import org.guild.wars.objects.achievements.rewards.RewardTitle;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class AchievementDeserializer extends StdDeserializer<Achievement>
{
	private static final long serialVersionUID = 1L;

	public AchievementDeserializer()
	{
		this(null);
	}
	
	public AchievementDeserializer(Class<Achievement> vc)
	{
		super(vc);
	}

	@Override
	public Achievement deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException, JsonProcessingException
	{
		Achievement achievement = new Achievement();
		JsonNode node = parser.getCodec().readTree(parser);
		ObjectMapper mapper = (ObjectMapper) parser.getCodec();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(AchievementReward.class, new AchievementRewardDeserializer());
		mapper.registerModule(module);
		if(node.hasNonNull("id") && node.get("id").isInt())
		{
			achievement.setId(node.get("id").asText());
		}
		if(node.hasNonNull("icon") && node.get("icon").isTextual())
		{
			achievement.setIcon(node.get("icon").asText());
		}
		if(node.hasNonNull("name") && node.get("name").isTextual())
		{
			achievement.setName(node.get("name").asText());
		}
		if(node.hasNonNull("description") && node.get("description").isTextual())
		{
			achievement.setDescription(node.get("description").asText());
		}
		if(node.hasNonNull("requirement") && node.get("requirement").isTextual())
		{
			achievement.setRequirement(node.get("requirement").asText());
		}
		if(node.hasNonNull("locked_text") && node.get("locked_text").isTextual())
		{
			achievement.setLockedText(node.get("locked_text").asText());
		}
		if(node.hasNonNull("type") && node.get("type").isTextual())
		{
			AchievementType type = AchievementType.valueOf(node.get("type").asText());
			achievement.setType(type);
		}
		if(node.hasNonNull("flags") && node.get("flags").isArray())
		{
			AchievementTag[] flags = mapper.treeToValue(node.get("flags"), AchievementTag[].class);
			achievement.setFlags(flags);
		}
		if(node.hasNonNull("tiers") && node.get("tiers").isArray())
		{
			AchievementTiers[] tiers = mapper.treeToValue(node.get("tiers"), AchievementTiers[].class);
			achievement.setTiers(tiers);
		}
		if(node.hasNonNull("prerequisites") && node.get("id").isArray())
		{
			int[] prerequisites = mapper.treeToValue(node, int[].class);
			achievement.setPrerequisites(prerequisites);
		}
		if(node.hasNonNull("rewards") && node.get("rewards").isArray())
		{
			List<AchievementReward> listReward = handleRewardNode(node.get("rewards"), mapper);
			achievement.setRewards(listReward.toArray(new AchievementReward[0])); 
		}
		if(node.hasNonNull("bits") && node.get("bits").isArray())
		{
			InfoBit[] bits = mapper.treeToValue(node.get("bits"), InfoBit[].class);
			achievement.setBits(bits);
		}
		if(node.hasNonNull("point_cap") && node.get("point_cap").isInt())
		{
			achievement.setPointCap(node.get("point_cap").asInt());
		}
		return achievement;
	}
	
	private List<AchievementReward> handleRewardNode(JsonNode rewardNode, ObjectMapper mapper)
	{
		List<AchievementReward> listReward = new ArrayList<AchievementReward>();
		Consumer<JsonNode> populate = (JsonNode reward)  ->
		{
			try
			{
				if(reward.hasNonNull("type") && reward.get("type").isTextual())
				{
					AchievementRewardType type = AchievementRewardType.valueOf(reward.get("type").asText());
					switch(type)
					{
						case Coins:
							RewardCoins coins = (RewardCoins) mapper.treeToValue(reward, AchievementReward.class);
							listReward.add(coins);
							break;
						case Item:
							RewardItems item = (RewardItems) mapper.treeToValue(reward, AchievementReward.class);
							listReward.add(item);
							break;
						case Mastery:
							RewardMastery mastery = (RewardMastery) mapper.treeToValue(reward, AchievementReward.class);
							listReward.add(mastery);
							break;
						case Title:
							RewardTitle title = (RewardTitle) mapper.treeToValue(reward, AchievementReward.class);
							listReward.add(title);
							break;
						default:
							throw new IOException();
					}
				}
				else
					throw new IOException();
			}
			catch(JsonProcessingException e)
			{
				e.printStackTrace();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
			
		}; 
		rewardNode.forEach(populate);
		return listReward;
	}
}
