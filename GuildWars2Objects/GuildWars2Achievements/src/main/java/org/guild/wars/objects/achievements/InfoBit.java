package org.guild.wars.objects.achievements;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfoBit
{
	public enum InfoBitType
	{
		@JsonProperty("Text")
		Text("Text"),
		@JsonProperty("Item")
		Item("Item"),
		@JsonProperty("Minipet")
		MiniPet("MiniPet"),
		@JsonProperty("Skin")
		Skin("Skin");
		
		private final String value;
		
		InfoBitType(String value)
		{
			this.value = value;
		}
		
		public String getValue()
		{
			return this.value;
		}
	}
	
	@JsonProperty(value="type", required=true)
	private InfoBitType type;
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("text")
	private String text;

	/**
	 * @return the type
	 */
	public InfoBitType getType()
	{
		return type;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}
}
