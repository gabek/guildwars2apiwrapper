package org.guild.wars.objects.achievements.daily;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyLevel
{
	@JsonProperty(value="min", required=true)
	private int min;
	
	@JsonProperty(value="max", required=true)
	private int max;

	/**
	 * @return the min
	 */
	public int getMin()
	{
		return min;
	}

	/**
	 * @return the max
	 */
	public int getMax()
	{
		return max;
	}
}
