package org.guild.wars.objects.achievements.rewards;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class AchievementReward
{
	@JsonProperty(value="type", required=true)
	private AchievementRewardType type;

	

	/**
	 * @return the type
	 */
	public AchievementRewardType getType()
	{
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	protected void setType(AchievementRewardType type)
	{
		this.type = type;
	}
}
