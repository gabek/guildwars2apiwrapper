package org.guild.wars.objects.achievements.daily;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.common.Expansion;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class TestDailyAchievement
{
	private ObjectMapper mapper;
	private String dailyResource = "DailyAchievement";
	
	public TestDailyAchievement()
	{
		mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(DailyAchievement.class, new DailyAchievementDeserializer());
		mapper.registerModule(module);
	}
	
	@Test
	public void testDailyAchievement1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(dailyResource + "/test1.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievement result = mapper.readValue(daily, DailyAchievement.class);
		
		Expansion[] expectedExpansion = {Expansion.Base, Expansion.HeartOfThorns, Expansion.PathOfFire};
		
		assertEquals(1981, result.getId());
		assertEquals(1, result.getLevel().getMin());
		assertEquals(80, result.getLevel().getMax());
		assertEquals(expectedExpansion.length, result.getRequiredCampaign().length);
		for(int i = 0; i< expectedExpansion.length; i++)
			assertEquals(expectedExpansion[i], result.getRequiredCampaign()[i]);
	}
	
	@Test
	public void testDailyAchievement2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(dailyResource + "/test2.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievement result = mapper.readValue(daily, DailyAchievement.class);
		
		Expansion[] expectedExpansion = {Expansion.Base, Expansion.HeartOfThorns, Expansion.PathOfFire};
		
		assertEquals(3450, result.getId());
		assertEquals(31, result.getLevel().getMin());
		assertEquals(80, result.getLevel().getMax());
		assertEquals(expectedExpansion.length, result.getRequiredCampaign().length);
		for(int i = 0; i< expectedExpansion.length; i++)
			assertEquals(expectedExpansion[i], result.getRequiredCampaign()[i]);
	}
	
	@Test
	public void testDailyAchievement3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(dailyResource + "/test3.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievement result = mapper.readValue(daily, DailyAchievement.class);
		
		Expansion[] expectedExpansion = {Expansion.Base, Expansion.HeartOfThorns, Expansion.PathOfFire};
		
		assertEquals(1951, result.getId());
		assertEquals(11, result.getLevel().getMin());
		assertEquals(79, result.getLevel().getMax());
		assertEquals(expectedExpansion.length, result.getRequiredCampaign().length);
		for(int i = 0; i< expectedExpansion.length; i++)
			assertEquals(expectedExpansion[i], result.getRequiredCampaign()[i]);
	}
}
