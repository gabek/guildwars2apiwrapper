package org.guild.wars.objects.achievements;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.achievements.InfoBit.InfoBitType;
import org.guild.wars.objects.achievements.rewards.AchievementRewardType;
import org.guild.wars.objects.achievements.rewards.RewardCoins;
import org.guild.wars.objects.achievements.rewards.RewardItems;
import org.guild.wars.objects.achievements.rewards.RewardMastery;
import org.guild.wars.objects.achievements.rewards.RewardTitle;
import org.guild.wars.objects.common.Expansion;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * @author Gab
 * A test for achievements.
 */

public class TestAchievement
{
	private ObjectMapper mapper;
	private String achieveResource = "Achievement";
	
	public TestAchievement()
	{
		mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Achievement.class, new AchievementDeserializer());
		mapper.registerModule(module);
	}
	
	@Test
	public void testAchievement1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(achieveResource + "/test1.json");
		String achievement = IOUtils.toString(inputStream, "utf-8");
		Achievement result = mapper.readValue(achievement, Achievement.class);
		
		AchievementTag[] expectedFlags = {AchievementTag.PvP, AchievementTag.CategoryDisplay, AchievementTag.Daily};
		AchievementRewardType[] expectedRewards = {AchievementRewardType.Item, AchievementRewardType.Coins};
		
		assertEquals("1840", result.getId());
		assertEquals("Daily Completionist", result.getName());
		assertEquals("", result.getDescription());
		assertEquals("Complete any  PvE, WvW, or PvP Daily Achievements.", result.getRequirement());
		assertEquals("", result.getLockedText());
		assertEquals(AchievementType.Default, result.getType());
		assertEquals(expectedFlags.length, result.getFlags().length);
		for(int i = 0; i <expectedFlags.length; i++)
			assertEquals(expectedFlags[i], result.getFlags()[i]);
		assertEquals(1, result.getTiers().length);
		assertEquals(3, result.getTiers()[0].getCount());
		assertEquals(10, result.getTiers()[0].getPoints());
		assertEquals(expectedRewards.length, result.getRewards().length);
		for(int i = 0; i < expectedRewards.length; i++)
			assertEquals(expectedRewards[i], result.getRewards()[i].getType());
		
		RewardItems resultItem = (RewardItems) result.getRewards()[0];
		assertEquals(70047, resultItem.getId());
		assertEquals(1, resultItem.getCount());
		
		RewardCoins resultCoin = (RewardCoins) result.getRewards()[1];
		assertEquals(20000, resultCoin.getCount());
	}
	
	@Test
	public void testAchievement2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(achieveResource + "/test2.json");
		String achievement = IOUtils.toString(inputStream, "utf-8");
		Achievement result = mapper.readValue(achievement, Achievement.class);
		
		AchievementTag[] expectedFlags = {AchievementTag.CategoryDisplay};
		AchievementRewardType[] expectedRewards = {AchievementRewardType.Mastery, AchievementRewardType.Title};
		
		assertEquals("910", result.getId());
		assertEquals("Tequatl the Sunless", result.getName());
		assertEquals("", result.getDescription());
		assertEquals("Complete  Tequatl achievements.", result.getRequirement());
		assertEquals("", result.getLockedText());
		assertEquals(AchievementType.Default, result.getType());
		assertEquals(expectedFlags.length, result.getFlags().length);
		for(int i = 0; i <expectedFlags.length; i++)
			assertEquals(expectedFlags[i], result.getFlags()[i]);
		assertEquals(1, result.getTiers().length);
		assertEquals(10, result.getTiers()[0].getCount());
		assertEquals(50, result.getTiers()[0].getPoints());
		assertEquals(expectedRewards.length, result.getRewards().length);
		for(int i = 0; i < expectedRewards.length; i++)
			assertEquals(expectedRewards[i], result.getRewards()[i].getType());
		
		RewardMastery resultMastery = (RewardMastery) result.getRewards()[0];
		assertEquals(247, resultMastery.getId());
		assertEquals(Expansion.Base, resultMastery.getRegion());
		
		RewardTitle resultTitle = (RewardTitle) result.getRewards()[1];
		assertEquals(175, resultTitle.getId());
	}
	
	@Test
	public void testAchievement3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(achieveResource + "/test3.json");
		String achievement = IOUtils.toString(inputStream, "utf-8");
		Achievement result = mapper.readValue(achievement, Achievement.class);
		
		AchievementTag[] expectedFlags = {AchievementTag.IgnoreNearlyComplete, AchievementTag.PvP, AchievementTag.RepairOnLogin, AchievementTag.RequiresUnlock, AchievementTag.Permanent};
		AchievementRewardType[] expectedRewards = {AchievementRewardType.Item};
		InfoBitType[] expectedBitType = {InfoBitType.Skin, InfoBitType.Skin, InfoBitType.Skin, InfoBitType.Skin, InfoBitType.Item, InfoBitType.Item, InfoBitType.Item};
		int[] expectedBitId = {208,197,211,132,70502,70944,74774};
		int[] expectedCount = {1,3,7};
		int[] expectedPoints = {1,1,1};
		
		assertEquals("2258", result.getId());
		assertEquals("Mistward Legguards", result.getName());
		assertEquals("Test", result.getDescription());
		assertEquals("Collect all  items and skins to forge your Mistward Legguards.", result.getRequirement());
		assertEquals("Complete the Invocation revenant specialization to unlock the Mistward Legguards collection.", result.getLockedText());
		assertEquals(AchievementType.ItemSet, result.getType());
		assertEquals(expectedFlags.length, result.getFlags().length);
		for(int i = 0; i <expectedFlags.length; i++)
			assertEquals(expectedFlags[i], result.getFlags()[i]);
		assertEquals(expectedBitType.length, result.getBits().length);
		for(int i = 0; i < expectedBitType.length; i++)
		{
			assertEquals(expectedBitType[i], result.getBits()[i].getType());
			assertEquals(expectedBitId[i], result.getBits()[i].getId());
		}
		assertEquals(expectedCount.length, result.getTiers().length);
		for(int i = 0; i < expectedCount.length; i++)
		{
			assertEquals(expectedCount[i], result.getTiers()[i].getCount());
			assertEquals(expectedPoints[i], result.getTiers()[i].getPoints());
		}
		assertEquals(expectedRewards.length, result.getRewards().length);
		for(int i = 0; i < expectedRewards.length; i++)
			assertEquals(expectedRewards[i], result.getRewards()[i].getType());
		
		RewardItems resultItem = (RewardItems) result.getRewards()[0];
		assertEquals(70598, resultItem.getId());
		assertEquals(1, resultItem.getCount());
		
	}

}
