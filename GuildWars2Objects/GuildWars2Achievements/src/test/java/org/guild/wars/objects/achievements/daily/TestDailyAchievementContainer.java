package org.guild.wars.objects.achievements.daily;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class TestDailyAchievementContainer
{
	private ObjectMapper mapper;
	private String containerResource = "DailyContainer";
	
	public TestDailyAchievementContainer()
	{
		mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(DailyAchievement.class, new DailyAchievementDeserializer());
		mapper.registerModule(module);
	}
	
	@Test
	public void testDailyAchivement1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test1.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievementContainer result = mapper.readValue(daily, DailyAchievementContainer.class);
		
		int[] expectedPve = {1981, 1937, 1839, 1941, 1951, 1917, 1913};
		int[] expectedPvp = {1856, 1857, 3449, 3450};
		int[] expectedWvw = {1848, 437, 1851, 1843};
		int[] expectedFractals = {2473, 2309, 3219, 4028, 4006, 4030,
				3973, 2929, 2889, 2950, 2947, 2979, 2985, 2934, 2966
		};
		int[] expectedSpecials = {};
		
		assertEquals(expectedPve.length, result.getPve().length);
		for(int i = 0; i< expectedPve.length; i++)
			assertEquals(expectedPve[i], result.getPve()[i].getId());
		
		assertEquals(expectedPvp.length, result.getPvp().length);
		for(int i = 0; i< expectedPvp.length; i++)
			assertEquals(expectedPvp[i], result.getPvp()[i].getId());
		
		assertEquals(expectedWvw.length, result.getWvw().length);
		for(int i = 0; i< expectedWvw.length; i++)
			assertEquals(expectedWvw[i], result.getWvw()[i].getId());
		
		assertEquals(expectedFractals.length, result.getFractals().length);
		for(int i = 0; i< expectedFractals.length; i++)
			assertEquals(expectedFractals[i], result.getFractals()[i].getId());
		
		assertEquals(expectedSpecials.length, result.getSpecial().length);
		for(int i = 0; i< expectedSpecials.length; i++)
			assertEquals(expectedSpecials[i], result.getSpecial()[i].getId());
	}
	
	@Test
	public void testDailyAchivement2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test2.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievementContainer result = mapper.readValue(daily, DailyAchievementContainer.class);
		
		int[] expectedPve = {1974, 1973, 1936, 3814, 1959, 1953, 2987, 
				1930, 1930};
		int[] expectedPvp = {1857, 2816, 3450, 3449};
		int[] expectedWvw = {946, 1847, 1845, 1850};
		int[] expectedFractals = {2597, 2171, 3201, 2935, 2961, 2972,
				2892, 3509, 3478, 3464, 3458, 2911, 2908, 2890, 2989
		};
		int[] expectedSpecials = {};
		
		assertEquals(expectedPve.length, result.getPve().length);
		for(int i = 0; i< expectedPve.length; i++)
			assertEquals(expectedPve[i], result.getPve()[i].getId());
		
		assertEquals(expectedPvp.length, result.getPvp().length);
		for(int i = 0; i< expectedPvp.length; i++)
			assertEquals(expectedPvp[i], result.getPvp()[i].getId());
		
		assertEquals(expectedWvw.length, result.getWvw().length);
		for(int i = 0; i< expectedWvw.length; i++)
			assertEquals(expectedWvw[i], result.getWvw()[i].getId());
		
		assertEquals(expectedFractals.length, result.getFractals().length);
		for(int i = 0; i< expectedFractals.length; i++)
			assertEquals(expectedFractals[i], result.getFractals()[i].getId());
		
		assertEquals(expectedSpecials.length, result.getSpecial().length);
		for(int i = 0; i< expectedSpecials.length; i++)
			assertEquals(expectedSpecials[i], result.getSpecial()[i].getId());
	}
	
	@Test
	public void testDailyAchivement3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test3.json");
		String daily = IOUtils.toString(inputStream, "utf-8");
		DailyAchievementContainer result = mapper.readValue(daily, DailyAchievementContainer.class);
		
		int[] expectedPve = {1980, 1975, 3562, 1932, 1839, 1967, 1964, 
				2022};
		int[] expectedPvp = {1861, 2817, 3449, 3450};
		int[] expectedWvw = {437, 1849, 1844, 1851};
		int[] expectedFractals = {2422, 2533, 3185, 2916, 2949, 2918,
				2952, 2981, 2991, 2895, 2967, 2900, 2898, 2964, 2956
		};
		int[] expectedSpecials = {};
		
		assertEquals(expectedPve.length, result.getPve().length);
		for(int i = 0; i< expectedPve.length; i++)
			assertEquals(expectedPve[i], result.getPve()[i].getId());
		
		assertEquals(expectedPvp.length, result.getPvp().length);
		for(int i = 0; i< expectedPvp.length; i++)
			assertEquals(expectedPvp[i], result.getPvp()[i].getId());
		
		assertEquals(expectedWvw.length, result.getWvw().length);
		for(int i = 0; i< expectedWvw.length; i++)
			assertEquals(expectedWvw[i], result.getWvw()[i].getId());
		
		assertEquals(expectedFractals.length, result.getFractals().length);
		for(int i = 0; i< expectedFractals.length; i++)
			assertEquals(expectedFractals[i], result.getFractals()[i].getId());
		
		assertEquals(expectedSpecials.length, result.getSpecial().length);
		for(int i = 0; i< expectedSpecials.length; i++)
			assertEquals(expectedSpecials[i], result.getSpecial()[i].getId());
	}
}
