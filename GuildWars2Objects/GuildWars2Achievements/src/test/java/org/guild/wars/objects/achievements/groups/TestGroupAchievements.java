package org.guild.wars.objects.achievements.groups;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestGroupAchievements
{
	private ObjectMapper mapper;
	private String groupResource = "GroupAchievements";
	
	public TestGroupAchievements()
	{
		mapper = new ObjectMapper();
	}
	
	@Test
	public void testGroupAchievement1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(groupResource + "/test1.json");
		String group = IOUtils.toString(inputStream, "utf-8");
		AchievementGroups result = mapper.readValue(group, AchievementGroups.class);
		
		int[] expectedCategories = {108, 109, 110, 111,112,116};
		
		assertEquals("65B4B678-607E-4D97-B458-076C3E96A810", result.getId());
		assertEquals("Heart of Thorns", result.getName());
		assertEquals("Achievements for accomplishments throughout the jungle.",result.getDescription());
		assertEquals("6", result.getOrder());
		assertEquals(expectedCategories.length, result.getCategories().length);
		for(int i=0; i<expectedCategories.length; i++)
			assertEquals(expectedCategories[i], result.getCategories()[i]);
	}
	
	@Test
	public void testGroupAchievement2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(groupResource + "/test2.json");
		String group = IOUtils.toString(inputStream, "utf-8");
		AchievementGroups result = mapper.readValue(group, AchievementGroups.class);
		
		int[] expectedCategories = {148, 170, 30};
		
		assertEquals("4E6A6CE7-B131-40BB-81A3-235CDBACDAA9", result.getId());
		assertEquals("Fractals of the Mists", result.getName());
		assertEquals("Achievements related to the Fractals of the Mists.",result.getDescription());
		assertEquals("10", result.getOrder());
		assertEquals(expectedCategories.length, result.getCategories().length);
		for(int i=0; i<expectedCategories.length; i++)
			assertEquals(expectedCategories[i], result.getCategories()[i]);
	}
	
	@Test
	public void testGroupAchievement3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(groupResource + "/test3.json");
		String group = IOUtils.toString(inputStream, "utf-8");
		AchievementGroups result = mapper.readValue(group, AchievementGroups.class);
		
		int[] expectedCategories = {169, 10, 1, 133, 5, 2, 27, 58, 7, 51, 47, 14,
				6, 11, 16, 4, 80, 69, 206};
		
		assertEquals("56A82BB9-6B07-4AB0-89EE-E4A6D68F5C47", result.getId());
		assertEquals("General", result.getName());
		assertEquals("Achievements for accomplishments throughout Central Tyria.",result.getDescription());
		assertEquals("4", result.getOrder());
		assertEquals(expectedCategories.length, result.getCategories().length);
		for(int i=0; i<expectedCategories.length; i++)
			assertEquals(expectedCategories[i], result.getCategories()[i]);
	}
	
}
