package org.guild.wars.objects.dungeons;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DungeonTest
{
	private ObjectMapper mapper;
	
	public DungeonTest()
	{
		this.mapper = new ObjectMapper();
	}
	
	@Test
	public void testDungeon1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test1.json");
		String dungeonJson = IOUtils.toString(inputStream, "utf-8");
		Dungeon result = mapper.readValue(dungeonJson, Dungeon.class);
		
		String[] expectedName = {"cm_story", "asura", "seraph", "butler"};
		DungeonType[] expectedType = {DungeonType.Story, DungeonType.Explorable, DungeonType.Explorable,
				DungeonType.Explorable};
		
		assertEquals(DungeonId.CaudecusManor, result.getId());
		assertEquals(4, result.getPaths().length);
		
		for(int i = 0; i < result.getPaths().length; i++)
		{
			assertEquals(expectedName[i], result.getPaths()[i].getId());
			assertEquals(expectedType[i], result.getPaths()[i].getType());
		}
	}
	
	@Test
	public void testDungeon2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test2.json");
		String dungeonJson = IOUtils.toString(inputStream, "utf-8");
		Dungeon result = mapper.readValue(dungeonJson, Dungeon.class);
		
		String[] expectedName = {"cof_story", "ferrah", "magg", "rhiannon"};
		DungeonType[] expectedType = {DungeonType.Story, DungeonType.Explorable, DungeonType.Explorable,
				DungeonType.Explorable};
		
		assertEquals(DungeonId.CitadelOfFlame, result.getId());
		assertEquals(4, result.getPaths().length);
		
		for(int i = 0; i < result.getPaths().length; i++)
		{
			assertEquals(expectedName[i], result.getPaths()[i].getId());
			assertEquals(expectedType[i], result.getPaths()[i].getType());
		}
	}
	
	@Test
	public void testDungeon3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test3.json");
		String dungeonJson = IOUtils.toString(inputStream, "utf-8");
		Dungeon result = mapper.readValue(dungeonJson, Dungeon.class);
		
		String[] expectedName = {"coe_story", "submarine", "teleporter", "front_door"};
		DungeonType[] expectedType = {DungeonType.Story, DungeonType.Explorable, DungeonType.Explorable,
				DungeonType.Explorable};
		
		assertEquals(DungeonId.CrucibleOfEternity, result.getId());
		assertEquals(4, result.getPaths().length);
		
		for(int i = 0; i < result.getPaths().length; i++)
		{
			assertEquals(expectedName[i], result.getPaths()[i].getId());
			assertEquals(expectedType[i], result.getPaths()[i].getType());
		}
	}
}
