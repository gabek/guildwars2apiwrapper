package org.guild.wars.objects.dungeons;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DungeonPath
{
	@JsonProperty(value="id", required=true)
	private String id;
	
	@JsonProperty(value="type",required=true)
	private DungeonType type;

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the type
	 */
	public DungeonType getType()
	{
		return type;
	}
	
	
}
