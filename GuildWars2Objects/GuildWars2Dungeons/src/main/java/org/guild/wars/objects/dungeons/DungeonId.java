package org.guild.wars.objects.dungeons;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DungeonId
{
	@JsonProperty("ascalonian_catacombs")
	AscalonianCatacombs ("AscalonianCatacombs"),
	@JsonProperty("caudecus_manor")
	CaudecusManor ("CaudecusManor"),
	@JsonProperty("twilight_arbor")
	TwilightArbor ("TwilightArbor"),
	@JsonProperty("sorrows_embrace")
	SorrowsEmbrace ("SorrowsEmbrace"),
	@JsonProperty("citadel_of_flame")
	CitadelOfFlame ("CitadelOfFlame"),
	@JsonProperty("honor_of_the_waves")
	HonorOfTheWaves ("HonorOfTheWaves"),
	@JsonProperty("crucible_of_eternity")
	CrucibleOfEternity ("CrucibleOfEternity"),
	@JsonProperty("ruined_city_of_arah")
	RuinedCityOfArah ("RuinedCityOfArah");
	
	private final String name;
	
	DungeonId(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
