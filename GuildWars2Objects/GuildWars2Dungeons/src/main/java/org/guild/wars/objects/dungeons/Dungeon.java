package org.guild.wars.objects.dungeons;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Dungeon
{
	@JsonProperty(value="id", required=true)
	private DungeonId id;
	
	@JsonProperty(value="paths", required=true)
	private DungeonPath[] paths;

	/**
	 * @return the id
	 */
	public DungeonId getId()
	{
		return id;
	}

	/**
	 * @return the paths
	 */
	public DungeonPath[] getPaths()
	{
		return paths;
	}
	
	
}
