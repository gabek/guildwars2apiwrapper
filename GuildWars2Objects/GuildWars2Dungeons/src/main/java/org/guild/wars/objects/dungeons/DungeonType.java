package org.guild.wars.objects.dungeons;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum DungeonType
{
	@JsonProperty("Story")
	Story ("Story"),
	@JsonProperty("Explorable")
	Explorable ("Explorable");
	
	private final String value;
	
	DungeonType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
