package org.guild.wars.objects.items;

import org.guild.wars.objects.items.common.InfixUpgrade;
import org.guild.wars.objects.items.common.InfusionSlots;
import org.guild.wars.objects.items.weapons.DamageType;
import org.guild.wars.objects.items.weapons.WeaponType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Weapon extends Details
{
	@JsonProperty(value="type", required=true)
	private WeaponType type;
	
	@JsonProperty(value="damage_type", required=true)
	private DamageType damageType;
	
	@JsonProperty(value="min_power", required=true)
	private int minPower;
	
	@JsonProperty(value="max_power", required=true)
	private int maxPower;
	
	@JsonProperty(value="defense", required=true)
	private int defense;
	
	@JsonProperty(value="infusion_slots", required=true)
	private InfusionSlots[] infusionSlots;
	
	@JsonProperty("infix_upgrade")
	private InfixUpgrade infixUpgrade;
	
	@JsonProperty("suffix_item_id")
	private int suffixItemId;
	
	@JsonProperty(value="secondary_suffix_item_id", required=true)
	private String secondarySuffixItemId;
	
	@JsonProperty("stat_choices")
	private int[] statChoices;

	/**
	 * @return the type
	 */
	public WeaponType getType()
	{
		return type;
	}

	/**
	 * @return the damageType
	 */
	public DamageType getDamageType()
	{
		return damageType;
	}

	/**
	 * @return the minPower
	 */
	public int getMinPower()
	{
		return minPower;
	}

	/**
	 * @return the maxPower
	 */
	public int getMaxPower()
	{
		return maxPower;
	}

	/**
	 * @return the defense
	 */
	public int getDefense()
	{
		return defense;
	}

	/**
	 * @return the infusionSlots
	 */
	public InfusionSlots[] getInfusionSlots()
	{
		return infusionSlots;
	}

	/**
	 * @return the infixUpgrade
	 */
	public InfixUpgrade getInfixUpgrade()
	{
		return infixUpgrade;
	}

	/**
	 * @return the suffixItemId
	 */
	public int getSuffixItemId()
	{
		return suffixItemId;
	}

	/**
	 * @return the secondarySuffixItemId
	 */
	public String getSecondarySuffixItemId()
	{
		return secondarySuffixItemId;
	}

	/**
	 * @return the statChoices
	 */
	public int[] getStatChoices()
	{
		return statChoices;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(WeaponType type)
	{
		this.type = type;
	}

	/**
	 * @param damageType the damageType to set
	 */
	protected void setDamageType(DamageType damageType)
	{
		this.damageType = damageType;
	}

	/**
	 * @param minPower the minPower to set
	 */
	protected void setMinPower(int minPower)
	{
		this.minPower = minPower;
	}

	/**
	 * @param maxPower the maxPower to set
	 */
	protected void setMaxPower(int maxPower)
	{
		this.maxPower = maxPower;
	}

	/**
	 * @param defense the defense to set
	 */
	protected void setDefense(int defense)
	{
		this.defense = defense;
	}

	/**
	 * @param infusionSlots the infusionSlots to set
	 */
	protected void setInfusionSlots(InfusionSlots[] infusionSlots)
	{
		this.infusionSlots = infusionSlots;
	}

	/**
	 * @param infixUpgrade the infixUpgrade to set
	 */
	protected void setInfixUpgrade(InfixUpgrade infixUpgrade)
	{
		this.infixUpgrade = infixUpgrade;
	}

	/**
	 * @param suffixItemId the suffixItemId to set
	 */
	protected void setSuffixItemId(int suffixItemId)
	{
		this.suffixItemId = suffixItemId;
	}

	/**
	 * @param secondarySuffixItemId the secondarySuffixItemId to set
	 */
	protected void setSecondarySuffixItemId(String secondarySuffixItemId)
	{
		this.secondarySuffixItemId = secondarySuffixItemId;
	}

	/**
	 * @param statChoices the statChoices to set
	 */
	protected void setStatChoices(int[] statChoices)
	{
		this.statChoices = statChoices;
	}
	
}
