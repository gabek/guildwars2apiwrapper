package org.guild.wars.objects.items;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Bag extends Details
{
	@JsonProperty(value="size", required=true)
	private int size;
	
	@JsonProperty(value="no_sell_or_sort", required=true)
	private boolean invisible;

	/**
	 * @return the size
	 */
	public int getSize()
	{
		return size;
	}

	/**
	 * @return if its invisible
	 */
	public boolean isInvisible()
	{
		return invisible;
	}
	
	
}
