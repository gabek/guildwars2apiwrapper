package org.guild.wars.objects.items;

import org.guild.wars.objects.items.container.ContainerType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Container extends Details
{
	@JsonProperty(value="type", required=true)
	private ContainerType type;

	
	/**
	 * @return the type
	 */
	public ContainerType getType()
	{
		return type;
	}
	
	/**
	 * @param type the type to set
	 */
	protected void setType(ContainerType type)
	{
		this.type = type;
	}

}
