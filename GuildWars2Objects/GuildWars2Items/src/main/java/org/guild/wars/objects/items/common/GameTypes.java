package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GameTypes
{
	@JsonProperty("Activity")
	Activity ("Activity"),
	@JsonProperty("Dungeon")
	Dungeon ("Dungeon"),
	@JsonProperty("Pve")
	PvE ("Pve"),
	@JsonProperty("Pvp")
	PvP ("Pvp"),
	@JsonProperty("PvpLobby")
	PvPLobby ("PvpLobby"),
	@JsonProperty("Wvw")
	WvW ("Wvw");
	
	private final String name;
	
	GameTypes(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
