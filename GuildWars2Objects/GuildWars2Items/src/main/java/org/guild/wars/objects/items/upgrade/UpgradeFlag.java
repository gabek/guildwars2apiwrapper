/**
 * 
 */
package org.guild.wars.objects.items.upgrade;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Gab
 *
 */
public enum UpgradeFlag
{
	@JsonProperty("Axe")
	Axe ("Axe"),
	@JsonProperty("Dagger")
	Dagger ("Dagger"),
	@JsonProperty("Focus")
	Focus ("Focus"),
	@JsonProperty("Greatsword")
	Greatsword ("Greatsword"),
	@JsonProperty("Hammer")
	Hammer ("Hammer"),
	@JsonProperty("Harpoon")
	Harpoon ("Harpoon"),
	@JsonProperty("LongBow")
	LongBow ("LongBow"),
	@JsonProperty("Mace")
	Mace ("Mace"),
	@JsonProperty("Pistol")
	Pistol ("Pistol"),
	@JsonProperty("Rifle")
	Rifle ("Rifle"),
	@JsonProperty("Scepter")
	Scepter ("Scepter"),
	@JsonProperty("Shield")
	Shield ("Shield"),
	@JsonProperty("ShortBow")
	ShortBow ("ShortBow"),
	@JsonProperty("Speargun")
	Speargun ("Speargun"),
	@JsonProperty("Staff")
	Staff ("Staff"),
	@JsonProperty("Sword")
	Sword ("Sword"),
	@JsonProperty("Torch")
	Torch ("Torch"),
	@JsonProperty("Trident")
	Trident ("Trident"),
	@JsonProperty("Warhorn")
	Warhorn ("Warhorn"),
	@JsonProperty("HeavyArmor")
	HeavyArmor ("HeavyArmor"),
	@JsonProperty("MediumArmor")
	MediumArmor ("MediumArmor"),
	@JsonProperty("LightArmor")
	LightArmor ("LightArmor"),
	@JsonProperty("Trinket")
	Trinket ("Trinket");
	
	private final String value;
	
	UpgradeFlag(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
	
}
