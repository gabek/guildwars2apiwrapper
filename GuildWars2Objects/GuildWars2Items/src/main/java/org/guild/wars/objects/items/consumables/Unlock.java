package org.guild.wars.objects.items.consumables;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Unlock
{
	@JsonProperty("BagSlot")
	BagSlot ("BagSlot"),
	@JsonProperty("BankTab")
	BankTab ("BankTab"),
	@JsonProperty("Champion")
	Champion ("Champion"),
	@JsonProperty("CollectibleCapacity")
	CollectibleCapacity ("CollectibleCapacity"),
	@JsonProperty("Content")
	Content ("Content"),
	@JsonProperty("CraftingRecipe")
	CraftingRecipe ("CraftingRecipe"),
	@JsonProperty("Dye")
	Dye ("Dye"),
	@JsonProperty("GliderSkin")
	Glider ("Glider"),
	@JsonProperty("Minipet")
	Miniatures ("Miniatures"),
	@JsonProperty("Ms")
	MountSkin ("MountSkin"),
	@JsonProperty("Outfit")
	Outfit ("Outfit"),
	@JsonProperty("RandomUnlock")
	Random ("Random"),
	@JsonProperty("SharedSlot")
	SharedSlot ("SharedSlot");
	
	private final String name;
	
	Unlock(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
