package org.guild.wars.objects.items;

import org.guild.wars.objects.items.gathering.GatheringType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GatheringTool extends Details
{
	@JsonProperty(value="type", required=true)
	private GatheringType type;

	/**
	 * @return the type
	 */
	public GatheringType getType()
	{
		return type;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(GatheringType type)
	{
		this.type = type;
	}
	
	
}
