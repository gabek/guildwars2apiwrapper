/**
 * 
 */
package org.guild.wars.objects.items;

import org.guild.wars.objects.items.common.InfixUpgrade;
import org.guild.wars.objects.items.common.InfusionSlots;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Gab
 */
public class BackItem extends Details
{
	@JsonProperty("infusion_slots")
	private InfusionSlots[] infusionSlots;
	
	@JsonProperty("infix_upgrade")
	private InfixUpgrade infixUpgrade;
	
	@JsonProperty("suffix_item_id")
	private int suffixItemId;
	
	@JsonProperty(value="secondary_suffix_item_id", required=true)
	private String secondarySuffixItemId;
	
	@JsonProperty("stat_choices")
	private int[] statChoices;

	/**
	 * @return the infusionSlots
	 */
	public InfusionSlots[] getInfusionSlots()
	{
		return infusionSlots;
	}

	/**
	 * @return the infixUpgrade
	 */
	public InfixUpgrade getInfixUpgrade()
	{
		return infixUpgrade;
	}

	/**
	 * @return the suffixItemId
	 */
	public int getSuffixItemId()
	{
		return suffixItemId;
	}

	/**
	 * @return the secondarySuffixItemId
	 */
	public String getSecondarySuffixItemId()
	{
		return secondarySuffixItemId;
	}

	/**
	 * @return the statChoices
	 */
	public int[] getStatChoices()
	{
		return statChoices;
	}
	
	
}
