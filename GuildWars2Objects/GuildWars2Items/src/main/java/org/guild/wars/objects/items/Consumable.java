package org.guild.wars.objects.items;

import org.guild.wars.objects.items.consumables.ConsumableType;
import org.guild.wars.objects.items.consumables.Unlock;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Consumable extends Details
{
	@JsonProperty(value="type", required=true)
	private ConsumableType type;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("duration_ms")
	private long duration;
	
	@JsonProperty("unlock_type")
	private Unlock unlock;
	
	@JsonProperty("color_id")
	private int colorId;
	
	@JsonProperty("recipe_id")
	private int recipeId;
	
	@JsonProperty("extra_recipe_ids")
	private int[] recipeIdArray;
	
	@JsonProperty("guild_upgrade_id")
	private int guildUpgradeId;
	
	@JsonProperty("apply_count")
	private int applyCount;
	
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("icon")
	private String icon;
	
	@JsonProperty("skins")
	private int[] skins;

	/**
	 * @return the type
	 */
	public ConsumableType getType()
	{
		return type;
	}

	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return the duration
	 */
	public long getDuration()
	{
		return duration;
	}

	/**
	 * @return the unlock
	 */
	public Unlock getUnlock()
	{
		return unlock;
	}

	/**
	 * @return the colorId
	 */
	public int getColorId()
	{
		return colorId;
	}

	/**
	 * @return the recipeId
	 */
	public int getRecipeId()
	{
		return recipeId;
	}

	/**
	 * @return the recipeIdArray
	 */
	public int[] getRecipeIdArray()
	{
		return recipeIdArray;
	}

	/**
	 * @return the guildUpgradeId
	 */
	public int getGuildUpgradeId()
	{
		return guildUpgradeId;
	}

	/**
	 * @return the applyCount
	 */
	public int getApplyCount()
	{
		return applyCount;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @return the icon
	 */
	public String getIcon()
	{
		return icon;
	}

	/**
	 * @return the skins
	 */
	public int[] getSkins()
	{
		return skins;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(ConsumableType type)
	{
		this.type = type;
	}

	/**
	 * @param description the description to set
	 */
	protected void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param duration the duration to set
	 */
	protected void setDuration(long duration)
	{
		this.duration = duration;
	}

	/**
	 * @param unlock the unlock to set
	 */
	protected void setUnlock(Unlock unlock)
	{
		this.unlock = unlock;
	}

	/**
	 * @param colorId the colorId to set
	 */
	protected void setColorId(int colorId)
	{
		this.colorId = colorId;
	}

	/**
	 * @param recipeId the recipeId to set
	 */
	protected void setRecipeId(int recipeId)
	{
		this.recipeId = recipeId;
	}

	/**
	 * @param recipeIdArray the recipeIdArray to set
	 */
	protected void setRecipeIdArray(int[] recipeIdArray)
	{
		this.recipeIdArray = recipeIdArray;
	}

	/**
	 * @param guildUpgradeId the guildUpgradeId to set
	 */
	protected void setGuildUpgradeId(int guildUpgradeId)
	{
		this.guildUpgradeId = guildUpgradeId;
	}

	/**
	 * @param applyCount the applyCount to set
	 */
	protected void setApplyCount(int applyCount)
	{
		this.applyCount = applyCount;
	}

	/**
	 * @param name the name to set
	 */
	protected void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param icon the icon to set
	 */
	protected void setIcon(String icon)
	{
		this.icon = icon;
	}

	/**
	 * @param skins the skins to set
	 */
	protected void setSkins(int[] skins)
	{
		this.skins = skins;
	}
	
	
}
