package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * TODO: change flags to something sensible? Array of 1 really?
 */

public class InfusionSlots
{
	@JsonProperty(value="flags", required=true)
	private InfixInfusion[] flags;
	
	@JsonProperty("item_id")
	private int id;

	/**
	 * @return the flags
	 */
	public InfixInfusion[] getFlags()
	{
		return flags;
	}

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	
	
}
