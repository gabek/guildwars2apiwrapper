package org.guild.wars.objects.items.consumables;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ConsumableType
{
	@JsonProperty("AppearanceChange")
	AppearanceChange ("AppearanceChange"),
	@JsonProperty("Booze")
	Booze ("Booze"),
	@JsonProperty("ContractNpc")
	ContractNpc ("ContractNpc"),
	@JsonProperty("Currency")
	Currency ("Currency"),
	@JsonProperty("Food")
	Food ("Food"),
	@JsonProperty("Generic")
	Generic ("Generic"),
	@JsonProperty("Halloween")
	Halloween ("Halloween"),
	@JsonProperty("Immediate")
	Immediate ("Immediate"),
	@JsonProperty("MountRandomUnlock")
	MountRandomUnlock ("MountRandomUnlock"),
	@JsonProperty("RandomUnlock")
	RandomUnlock ("RandomUnlock"),
	@JsonProperty("Transmutation")
	Transmutation ("Transmutation"),
	@JsonProperty("Unlock")
	Unlock ("Unlock"),
	@JsonProperty("UpgradeRemoval")
	UpgradeRemoval ("UpgradeRemoval"),
	@JsonProperty("Utility")
	Utility ("Utility"),
	@JsonProperty("TeleportToFriend")
	TeleportToFriend ("TeleportToFriend");
	
	private final String name;
	
	ConsumableType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
