package org.guild.wars.objects.items;

import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.common.UpgradeItem;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A abstract class for a generic item in Guild Wars2.
 * @author Gab
 */

public abstract class AbstractItem
{
	@JsonProperty(value="id", required=true)
	private int id;
	
	@JsonProperty(value="chat_link", required=true)
	private String chatLink;
	
	@JsonProperty(value="name", required=true)
	private String name;
	
	@JsonProperty(value="icon", required=true)
	private String icon;
	
	@JsonProperty("description")
	private String description;
	
	@JsonProperty(value="type", required=true)
	private ItemType type;
	
	@JsonProperty(value="rarity", required=true)
	private Rarity rarity;
	
	@JsonProperty(value="level", required=true)
	private int level;
	
	@JsonProperty(value="vendor_value", required=true)
	private int vendorValue;
	
	@JsonProperty("default_skin")
	private int defaultSkin;
	
	@JsonProperty(value="flags", required=true)
	private Flags[] flags;
	
	@JsonProperty(value="game_types", required=true)
	private GameTypes[] gameTypes;
	
	@JsonProperty(value="restrictions", required=true)
	private Restrictions[] restrictions;
	
	@JsonProperty("details")
	private Details details;
	
	@JsonProperty("upgrades_from")
	private UpgradeItem[] upgradesFrom;
	
	@JsonProperty("upgrades_into")
	private UpgradeItem[] upgradesInto;
	
	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}
	
	/**
	 * @return the chatLink
	 */
	public String getChatLink()
	{
		return chatLink;
	}
	
	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * @return the icon
	 */
	public String getIcon()
	{
		return icon;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}
	
	/**
	 * @return the type
	 */
	public ItemType getType()
	{
		return type;
	}
	
	/**
	 * @return the rarity
	 */
	public Rarity getRarity()
	{
		return rarity;
	}
	
	/**
	 * @return the level
	 */
	public int getLevel()
	{
		return level;
	}
	
	/**
	 * @return the vendorValue
	 */
	public int getVendorValue()
	{
		return vendorValue;
	}
	
	/**
	 * @return the defaultSkin
	 */
	public int getDefaultSkin()
	{
		return defaultSkin;
	}
	
	/**
	 * @return the flags
	 */
	public Flags[] getFlags()
	{
		return flags;
	}
	
	/**
	 * @return the gameTypes
	 */
	public GameTypes[] getGameTypes()
	{
		return gameTypes;
	}

	/**
	 * @return the restrictions
	 */
	public Restrictions[] getRestrictions()
	{
		return restrictions;
	}
	
	/**
	 * @return the details
	 */
	public Details getDetails()
	{
		return details;
	}

	/**
	 * @return the upgradesFrom
	 */
	public UpgradeItem[] getUpgradesFrom()
	{
		return upgradesFrom;
	}

	/**
	 * @return the upgradesInto
	 */
	public UpgradeItem[] getUpgradesInto()
	{
		return upgradesInto;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(int id)
	{
		this.id = id;
	}

	/**
	 * @param chatLink the chatLink to set
	 */
	protected void setChatLink(String chatLink)
	{
		this.chatLink = chatLink;
	}

	/**
	 * @param name the name to set
	 */
	protected void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @param icon the icon to set
	 */
	protected void setIcon(String icon)
	{
		this.icon = icon;
	}

	/**
	 * @param description the description to set
	 */
	protected void setDescription(String description)
	{
		this.description = description;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(ItemType type)
	{
		this.type = type;
	}

	/**
	 * @param rarity the rarity to set
	 */
	protected void setRarity(Rarity rarity)
	{
		this.rarity = rarity;
	}

	/**
	 * @param level the level to set
	 */
	protected void setLevel(int level)
	{
		this.level = level;
	}

	/**
	 * @param vendorValue the vendorValue to set
	 */
	protected void setVendorValue(int vendorValue)
	{
		this.vendorValue = vendorValue;
	}

	/**
	 * @param defaultSkin the defaultSkin to set
	 */
	protected void setDefaultSkin(int defaultSkin)
	{
		this.defaultSkin = defaultSkin;
	}

	/**
	 * @param flags the flags to set
	 */
	protected void setFlags(Flags[] flags)
	{
		this.flags = flags;
	}

	/**
	 * @param gameTypes the gameTypes to set
	 */
	protected void setGameTypes(GameTypes[] gameTypes)
	{
		this.gameTypes = gameTypes;
	}

	/**
	 * @param restrictions the restrictions to set
	 */
	protected void setRestrictions(Restrictions[] restrictions)
	{
		this.restrictions = restrictions;
	}

	/**
	 * @param details the details to set
	 */
	protected void setDetails(Details details)
	{
		this.details = details;
	}

	/**
	 * @param upgradesFrom the upgradesFrom to set
	 */
	protected void setUpgradesFrom(UpgradeItem[] upgradesFrom)
	{
		this.upgradesFrom = upgradesFrom;
	}

	/**
	 * @param upgradesInto the upgradesInto to set
	 */
	protected void setUpgradesInto(UpgradeItem[] upgradesInto)
	{
		this.upgradesInto = upgradesInto;
	}
	
	
}
