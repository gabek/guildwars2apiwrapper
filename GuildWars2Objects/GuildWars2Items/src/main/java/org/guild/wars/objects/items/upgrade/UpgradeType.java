package org.guild.wars.objects.items.upgrade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum UpgradeType
{
	@JsonProperty("Default")
	Default ("Default"),
	@JsonProperty("Gem")
	Gem ("Gem"),
	@JsonProperty("Rune")
	Rune ("Rune"),
	@JsonProperty("Sigil")
	Sigil ("Sigil");
	
	private final String value;
	
	UpgradeType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
