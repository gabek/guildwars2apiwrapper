package org.guild.wars.objects.items.armor;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ArmorWeight
{
	@JsonProperty("Heavy")
	Heavy ("Heavy"),
	@JsonProperty("Medium")
	Medium ("Medium"),
	@JsonProperty("Light")
	Light ("Light"),
	@JsonProperty("Clothing")
	Clothing ("Clothing");
	
	private final String name;
	
	ArmorWeight(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
