package org.guild.wars.objects.items.armor;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ArmorType
{
	@JsonProperty("Boots")
	Boots ("Boots"),
	@JsonProperty("Coat")
	Chest ("Chest"),
	@JsonProperty("Gloves")
	Gloves ("Gloves"),
	@JsonProperty("Helm")
	Helm ("Helm"),
	@JsonProperty("HelmAquatic")
	HelmAquatic ("HelmAquatic"),
	@JsonProperty("Leggings")
	Leggings ("Leggings"),
	@JsonProperty("Shoulders")
	Shoulders ("Shoulders");
	
	private final String name;
	
	ArmorType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
}
