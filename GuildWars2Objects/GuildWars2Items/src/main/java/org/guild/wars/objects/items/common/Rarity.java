package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Rarity
{
	@JsonProperty("Junk")
	Junk ("Junk"),
	@JsonProperty("Basic")
	Basic ("Basic"),
	@JsonProperty("Fine")
	Fine ("Fine"),
	@JsonProperty("Masterwork")
	Masterwork ("Masterwork"),
	@JsonProperty("Rare")
	Rare ("Rare"),
	@JsonProperty("Exotic")
	Exotic ("Exotic"),
	@JsonProperty("Ascended")
	Ascended ("Ascended"),
	@JsonProperty("Legendary")
	Legendary ("Legendary");
	
	private final String name;
	
	Rarity(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
