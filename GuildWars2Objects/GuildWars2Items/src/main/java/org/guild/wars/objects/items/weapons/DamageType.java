package org.guild.wars.objects.items.weapons;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * An enumeration for damage types.
 * The choking damage type isn't used but best to cover anyway just in case.
 */

public enum DamageType
{
	@JsonProperty("Fire")
	Fire ("Fire"),
	@JsonProperty("Ice")
	Ice ("Ice"),
	@JsonProperty("Lightning")
	Lightning ("Lightning"),
	@JsonProperty("Physical")
	Physical ("Physical"),
	@JsonProperty("Choking")
	Choking ("Choking");
	
	private final String name;
	
	DamageType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
