package org.guild.wars.objects.items;

import org.guild.wars.objects.items.armor.ArmorType;
import org.guild.wars.objects.items.armor.ArmorWeight;
import org.guild.wars.objects.items.common.InfixUpgrade;
import org.guild.wars.objects.items.common.InfusionSlots;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Armor extends Details
{
	@JsonProperty(value="type", required=true)
	private ArmorType armorType;
	
	@JsonProperty(value="weight_class", required=true)
	private ArmorWeight weight;
	
	@JsonProperty(value="defense", required=true)
	private int defense;
	
	@JsonProperty(value="infusion_slots", required=true)
	private InfusionSlots[] infusionSlots;

	@JsonProperty(value="infix_upgrade")
	private InfixUpgrade infixUpgrade;
	
	@JsonProperty("suffix_item_id")
	private int suffixItemId;
	
	@JsonProperty(value="secondary_suffix_item_id", required=true)
	private String secondarySuffixItemId;
	
	@JsonProperty("stat_choices")
	private int[] statChoices;
	
	/**
	 * @return the type
	 */
	public ArmorType getArmorType()
	{
		return armorType;
	}

	/**
	 * @return the weight
	 */
	public ArmorWeight getWeight()
	{
		return weight;
	}

	/**
	 * @return the defense
	 */
	public int getDefense()
	{
		return defense;
	}

	/**
	 * @return the infusionSlots
	 */
	public InfusionSlots[] getInfusionSlots()
	{
		return infusionSlots;
	}

	/**
	 * @return the infixUpgrade
	 */
	public InfixUpgrade getInfixUpgrade()
	{
		return infixUpgrade;
	}

	/**
	 * @return the suffixItemId
	 */
	public int getSuffixItemId()
	{
		return suffixItemId;
	}

	/**
	 * @return the secondarySuffixItemId
	 */
	public String getSecondarySuffixItemId()
	{
		return secondarySuffixItemId;
	}

	/**
	 * @return the statChoices
	 */
	public int[] getStatChoices()
	{
		return statChoices;
	}
}