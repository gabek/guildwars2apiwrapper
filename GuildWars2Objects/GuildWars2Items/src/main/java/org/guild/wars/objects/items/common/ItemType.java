package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 *
 */

public enum ItemType
{
	@JsonProperty("Armor")
	Armor ("Armor"),
	@JsonProperty("Bag")
	Bag ("Bag"),
	@JsonProperty("Back")
	BackItem ("BackItem"),
	@JsonProperty("Consumable")
	Consumable ("Consumable"),
	@JsonProperty("Container")
	Container ("Container"),
	@JsonProperty("CraftingMaterial")
	CraftingMaterial ("CraftingMaterial"),
	@JsonProperty("Gathering")
	GatheringTool ("GatheringTool"),
	@JsonProperty("Gizmo")
	Gizmo ("Gizmo"),
	@JsonProperty("MiniPet")
	Miniatures ("Minatures"),
	@JsonProperty("Tool")
	SalvageKits ("SalvageKits"),
	@JsonProperty("Trait")
	TraitGuides ("TraitGuides"),
	@JsonProperty("Trinket")
	Trinket ("Trinket"),
	@JsonProperty("Trophy")
	Trophies ("Trophies"),
	@JsonProperty("UpgradeComponent")
	UpgradeComponent ("UpgradeComponent"),
	@JsonProperty("Weapon")
	Weapon ("Weapon");
	
	private final String name;
	
	ItemType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public static ItemType convert(String name)
	{
		switch(name)
		{
			case "Armor": return ItemType.Armor;
			case "Bag": return ItemType.Bag;
			case "Back": return ItemType.BackItem;
			case "Consumable": return ItemType.Consumable;
			case "Container": return ItemType.Container;
			case "CraftingMaterial": return ItemType.CraftingMaterial;
			case "Gathering": return ItemType.GatheringTool;
			case "Gizmo": return ItemType.Gizmo;
			case "MiniPet": return ItemType.Miniatures;
			case "Tool": return ItemType.SalvageKits;
			case "Trait": return ItemType.TraitGuides;
			case "Trinket": return ItemType.Trinket;
			case "Trophy": return ItemType.Trophies;
			case "UpgradeComponent": return ItemType.UpgradeComponent;
			case "Weapon": return ItemType.Weapon;
			default: return null;
		}
		
	}
}
