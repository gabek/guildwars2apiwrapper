package org.guild.wars.objects.items.common;

import org.guild.wars.objects.items.AttributeName;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Attribute
{
	@JsonProperty(value="attribute", required=true)
	private AttributeName attribute;

	@JsonProperty(value="modifier", required=true)
	private int modifier;
	
	/*
	 * @return the attribute
	 */
	public AttributeName getAttribute()
	{
		return attribute;
	}

	/**
	 * @return the modifier
	 */
	public int getModifier()
	{
		return modifier;
	}
}
