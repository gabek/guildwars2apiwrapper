package org.guild.wars.objects.items;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 *
 */

public enum AttributeName
{
	@JsonProperty("AgonyResistance")
	AgonyResistance ("AgonyResistance"),
	@JsonProperty("BoonDuration")
	BoonDuration ("BoonDuration"),
	@JsonProperty("ConditionDamage")
	ConditionDamage ("ConditionDamage"),
	@JsonProperty("ConditionDuration")
	Expertise ("Expertise"),
	@JsonProperty("CritDamage")
	Ferocity ("Ferocity"),
	@JsonProperty("Healing")
	HealingPower ("Healing"),
	@JsonProperty("Power")
	Power ("Power"),
	@JsonProperty("Precision")
	Precision ("Precision"),
	@JsonProperty("Toughness")
	Toughness ("Toughness"),
	@JsonProperty("Vitality")
	Vitality ("Vitality");
	
	private final String value;
	
	AttributeName(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return value;
	}
}
