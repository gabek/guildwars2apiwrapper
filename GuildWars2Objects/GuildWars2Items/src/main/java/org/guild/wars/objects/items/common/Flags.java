package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Flags
{
	@JsonProperty("AccountBindOnUse")
	AccountBindOnUse ("AccountBindOnUse"),
	@JsonProperty("AccountBound")
	AccountBound ("AccountBound"),
	@JsonProperty("Attuned")
	Attuned ("Attuned"),
	@JsonProperty("BulkConsume")
	BulkConsume ("BulkConsume"),
	@JsonProperty("DeleteWarning")
	DeleteWarning ("DeleteWarning"),
	@JsonProperty("HideSuffix")
	HideSuffix ("HideSuffix"),
	@JsonProperty("Infused")
	Infused ("Infused"),
	@JsonProperty("MonsterOnly")
	MonsterOnly ("MonsterOnly"),
	@JsonProperty("NoMysticForge")
	NoMysticForge ("NoMysticForge"),
	@JsonProperty("NoSalvage")
	NoSalvage ("NoSalvage"),
	@JsonProperty("NoSell")
	NoSell ("NoSell"),
	@JsonProperty("NotUpgradeable")
	NotUpgradeable ("NotUpgradeable"),
	@JsonProperty("NoUnderwater")
	NoUnderwater ("NoUnderwater"),
	@JsonProperty("SoulbindOnAcquire")
	SoulbindOnAcquire ("SoulbindOnAcquire"),
	@JsonProperty("SoulBindOnUse")
	SoulBindOnUse ("SoulBindOnUse"),
	@JsonProperty("Tonic")
	Tonic ("Tonic"),
	@JsonProperty("Unique")
	Unique ("Unique");
	
	
	private final String name;
	
	Flags(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
