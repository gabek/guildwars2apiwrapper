package org.guild.wars.objects.items.trinket;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TrinketType
{
	@JsonProperty("Accessory")
	Accessory ("Accessory"),
	@JsonProperty("Amulet")
	Amulet ("Amulet"),
	@JsonProperty("Ring")
	Ring ("Ring");
	
	private final String value;
	
	TrinketType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
