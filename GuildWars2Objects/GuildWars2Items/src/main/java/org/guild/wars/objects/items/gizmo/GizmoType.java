package org.guild.wars.objects.items.gizmo;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GizmoType
{
	@JsonProperty("Default")
	Default ("Default"),
	@JsonProperty("ContainerKey")
	ContainerKey ("ContainerKey"),
	@JsonProperty("RentableContractNpc")
	RentableContractNpc ("RentableContractNpc"),
	@JsonProperty("UnlimitedConsumable")
	UnlimitedConsumable ("UnlimitedConsumable");
	
	private final String value;
	
	GizmoType(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
