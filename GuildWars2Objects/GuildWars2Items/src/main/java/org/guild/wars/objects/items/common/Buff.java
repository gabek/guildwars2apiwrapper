package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Buff
{
	@JsonProperty(value="skill_id", required=true)
	private String skillId;

	@JsonProperty(value="description", required=true)
	private String description;
		
		
	/**
	* @return the skillId
	*/
	public String getSkillId()
	{
		return skillId;
	}

	/**
	* @return the description
	*/
	public String getDescription()
	{
		return description;
	}
}
