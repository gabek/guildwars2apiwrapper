package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InfixUpgrade
{
	@JsonProperty(value="id", required=true)
	private int id;

	@JsonProperty(value="attributes", required=true)
	private Attribute[] attributes;

	@JsonProperty("buff")
	private Buff buff;

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @return the attributes
	 */
	public Attribute[] getAttributes()
	{
		return attributes;
	}

	/**
	 * @return the buff
	 */
	public Buff getBuff()
	{
		return buff;
	}

}
