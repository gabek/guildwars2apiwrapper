package org.guild.wars.objects.items;

import org.guild.wars.objects.items.common.InfixUpgrade;
import org.guild.wars.objects.items.upgrade.Infusion;
import org.guild.wars.objects.items.upgrade.UpgradeFlag;
import org.guild.wars.objects.items.upgrade.UpgradeType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpgradeComponent extends Details
{
	@JsonProperty(value="type", required=true)
	private UpgradeType type;
	
	@JsonProperty(value="flags", required=true)
	private UpgradeFlag[] flags;
	
	@JsonProperty(value="infusion_upgrade_flags",required=true)
	private Infusion[] infusions;
	
	@JsonProperty(value="suffix", required=true)
	private String suffix;
	
	@JsonProperty(value="infix_upgrade", required=true)
	private InfixUpgrade infixUpgrade;
	
	@JsonProperty("bonuses")
	private String[] bonuses;

	/**
	 * @return the type
	 */
	public UpgradeType getType()
	{
		return type;
	}

	/**
	 * @return the flags
	 */
	public UpgradeFlag[] getFlags()
	{
		return flags;
	}

	/**
	 * @return the infusions
	 */
	public Infusion[] getInfusions()
	{
		return infusions;
	}

	/**
	 * @return the suffix
	 */
	public String getSuffix()
	{
		return suffix;
	}

	/**
	 * @return the infixUpgrade
	 */
	public InfixUpgrade getInfixUpgrade()
	{
		return infixUpgrade;
	}

	/**
	 * @return the bonuses
	 */
	public String[] getBonuses()
	{
		return bonuses;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(UpgradeType type)
	{
		this.type = type;
	}

	/**
	 * @param flags the flags to set
	 */
	protected void setFlags(UpgradeFlag[] flags)
	{
		this.flags = flags;
	}

	/**
	 * @param infusions the infusions to set
	 */
	protected void setInfusions(Infusion[] infusions)
	{
		this.infusions = infusions;
	}

	/**
	 * @param suffix the suffix to set
	 */
	protected void setSuffix(String suffix)
	{
		this.suffix = suffix;
	}

	/**
	 * @param infixUpgrade the infixUpgrade to set
	 */
	protected void setInfixUpgrade(InfixUpgrade infixUpgrade)
	{
		this.infixUpgrade = infixUpgrade;
	}

	/**
	 * @param bonuses the bonuses to set
	 */
	protected void setBonuses(String[] bonuses)
	{
		this.bonuses = bonuses;
	}
}
