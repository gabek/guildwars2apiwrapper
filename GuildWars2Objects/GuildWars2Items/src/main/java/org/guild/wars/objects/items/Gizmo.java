package org.guild.wars.objects.items;

import org.guild.wars.objects.items.gizmo.GizmoType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Gizmo extends Details
{
	@JsonProperty(value="type", required=true)
	private GizmoType type;
	
	@JsonProperty("guild_upgrade_id")
	private int guildUpgradeId;

	/**
	 * @return the type
	 */
	public GizmoType getType()
	{
		return type;
	}

	/**
	 * @return the guildUpgradeId
	 */
	public int getGuildUpgradeId()
	{
		return guildUpgradeId;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(GizmoType type)
	{
		this.type = type;
	}

	/**
	 * @param guildUpgradeId the guildUpgradeId to set
	 */
	protected void setGuildUpgradeId(int guildUpgradeId)
	{
		this.guildUpgradeId = guildUpgradeId;
	}
	
}
