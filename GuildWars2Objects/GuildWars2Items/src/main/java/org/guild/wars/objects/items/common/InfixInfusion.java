package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum InfixInfusion
{
	@JsonProperty("Infusion")
	Infusion ("Infusion"),
	@JsonProperty("Enrichment")
	Enrichment ("Enrichment");
	
	private final String value;
	
	InfixInfusion(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
}
