package org.guild.wars.objects.items;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 
 * @author Gab
 * Type isn't an enum because there is only one value:Salvage.
 * Most likely there was plans for multiple salvage kits. 
 */

public class SalvageKits extends Details
{
	@JsonProperty(value="type",required=true)
	private String type;
	
	@JsonProperty(value="charges",required=true)
	private int charges;

	/**
	 * @return the type
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * @return the charges
	 */
	public int getCharges()
	{
		return charges;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(String type)
	{
		this.type = type;
	}

	/**
	 * @param charges the charges to set
	 */
	protected void setCharges(int charges)
	{
		this.charges = charges;
	}
}
