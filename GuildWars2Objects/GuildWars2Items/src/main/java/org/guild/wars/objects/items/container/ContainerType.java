package org.guild.wars.objects.items.container;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ContainerType
{
	@JsonProperty("Default")
	Default ("Default"),
	@JsonProperty("GiftBox")
	GiftBox ("GiftBox"),
	@JsonProperty("Immediate")
	Immediate ("Immediate"),
	@JsonProperty("OpenUI")
	OpenUI ("OpenUI");
	
	public final String name;
	
	ContainerType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	
}
