package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpgradeItem
{
	@JsonProperty("upgrade")
	private String upgrade;
	
	@JsonProperty("item_id")
	private int itemId;

	/**
	 * @return the upgrade
	 */
	public String getUpgrade()
	{
		return upgrade;
	}

	/**
	 * @return the itemId
	 */
	public int getItemId()
	{
		return itemId;
	}

}
