package org.guild.wars.objects.items.common;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Restrictions
{
	@JsonProperty("Asura")
	Asura ("Asura"),
	@JsonProperty("Charr")
	Charr ("Charr"),
	@JsonProperty("Human")
	Human ("Human"),
	@JsonProperty("Norn")
	Norn ("Norn"),
	@JsonProperty("Sylvari")
	Sylvari("Sylvari"),
	@JsonProperty("Elementalist")
	Elementalist ("Elementalist"),
	@JsonProperty("Engineer")
	Engineer ("Engineer"),
	@JsonProperty("Guardian")
	Guardian ("Guardian"),
	@JsonProperty("Mesmer")
	Mesmer ("Mesmer"),
	@JsonProperty("Necromancer")
	Necromancer ("Necromancer"),
	@JsonProperty("Ranger")
	Ranger ("Ranger"),
	@JsonProperty("Thief")
	Thief ("Thief"),
	@JsonProperty("Warrior")
	Warrior ("Warrior");
	
	private final String name;
	
	Restrictions(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
}
