package org.guild.wars.objects.items.weapons;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum WeaponType
{
	@JsonProperty("Axe")
	Axe ("Axe"),
	@JsonProperty("Dagger")
	Dagger ("Dagger"),
	@JsonProperty("Mace")
	Mace ("Mace"),
	@JsonProperty("Pistol")
	Pistol ("Pistol"),
	@JsonProperty("Scepter")
	Scepter ("Scepter"),
	@JsonProperty ("Focus")
	Focus ("Focus"),
	@JsonProperty ("Shield")
	Shield ("Shield"),
	@JsonProperty ("Torch")
	Torch ("Torch"),
	@JsonProperty ("Warhorn")
	Warhorn ("Warhorn"),
	@JsonProperty ("Greatsword")
	Greatsword ("Greatsword"),
	@JsonProperty ("Hammer")
	Hammer ("Hammer"),
	@JsonProperty ("LongBow")
	LongBow ("LongBow"),
	@JsonProperty ("Rifle")
	Rifle ("Rifle"),
	@JsonProperty ("ShortBow")
	ShortBow ("ShortBow"),
	@JsonProperty ("Staff")
	Staff ("Staff"),
	@JsonProperty ("Harpoon")
	Harpoon ("Harpoon"),
	@JsonProperty ("Speargun")
	Speargun ("Speargun"),
	@JsonProperty ("Trident")
	Trident ("Trident"),
	@JsonProperty("LargeBundle")
	LargeBundle ("LargeBundle"),
	@JsonProperty("SmallBundle")
	SmallBundle ("SmallBundle"),
	@JsonProperty("Toy")
	Toy ("Toy"),
	@JsonProperty("ToyTwoHanded")
	ToyTwoHanded ("ToyTwoHanded");
	
	private final String name;
	
	WeaponType(String name)
	{
		this.name = name;  
	}
	
	public String getName()
	{
		return this.name;
	}
}
