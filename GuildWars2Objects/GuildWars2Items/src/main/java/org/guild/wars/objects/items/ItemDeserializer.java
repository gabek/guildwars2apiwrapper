package org.guild.wars.objects.items;

import java.io.IOException;

import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.common.UpgradeItem;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * 
 * @author Gab
 * TODO: Build a testing component for this class.
 */

public class ItemDeserializer extends StdDeserializer<Item>
{
	private static final long serialVersionUID = 1L;

	public ItemDeserializer()
	{
		this(null);
	}
	public ItemDeserializer(Class<Item> vc)
	{
		super(vc);
	}

	@Override
	public Item deserialize(JsonParser parser, DeserializationContext context) throws IOException, JsonProcessingException
	{
		Item item = new Item();
		JsonNode node = parser.getCodec().readTree(parser);
		ObjectMapper mapper = (ObjectMapper) parser.getCodec();
		ItemType type = null;
		if(node.hasNonNull("id") && node.get("id").isInt())
		{
			item.setId(node.get("id").asInt());
		}
		if(node.hasNonNull("chat_link") && node.get("chat_link").isTextual())
		{
			item.setChatLink(node.get("chat_link").asText());
		}
		if(node.hasNonNull("name") && node.get("name").isTextual())
		{
			item.setName(node.get("name").asText());
		}
		if(node.hasNonNull("icon") && node.get("icon").isTextual())
		{
			item.setIcon(node.get("icon").asText());
		}
		if(node.hasNonNull("description") && node.get("description").isTextual())
		{
			item.setDescription(node.get("description").asText());
		}
		if(node.hasNonNull("type") && node.get("type").isTextual())
		{
			type = ItemType.convert(node.get("type").asText());
			item.setType(type);
		}
		if(node.hasNonNull("rarity") && node.get("rarity").isTextual())
		{
			Rarity rarity = Rarity.valueOf(node.get("rarity").asText());
			item.setRarity(rarity);
		}
		if(node.hasNonNull("level") && node.get("level").isInt())
		{
			item.setLevel(node.get("level").asInt());
		}
		if(node.hasNonNull("vendor_value") && node.get("vendor_value").isInt())
		{
			item.setVendorValue(node.get("vendor_value").asInt());
		}
		if(node.hasNonNull("default_skin") && node.get("default_skin").isInt())
		{
			item.setDefaultSkin(node.get("default_skin").asInt());
		}
		if(node.hasNonNull("flags") && node.get("flags").isArray())
		{
			Flags[] flags = mapper.treeToValue(node.get("flags"), Flags[].class);
			item.setFlags(flags);
		}
		if(node.hasNonNull("game_types") && node.get("game_types").isArray())
		{
			GameTypes[] gameTypes = mapper.treeToValue(node.get("game_types"), GameTypes[].class);
			item.setGameTypes(gameTypes);
		}
		if(node.hasNonNull("restrictions") && node.get("restrictions").isArray())
		{
			Restrictions[] restrictions = mapper.treeToValue(node.get("restrictions"), Restrictions[].class);
			item.setRestrictions(restrictions);
		}
		if(node.hasNonNull("upgrades_from") && node.get("upgrades_from").isArray())
		{
			UpgradeItem[] upgrades = mapper.treeToValue(node.get("upgrades_from"), UpgradeItem[].class);
			item.setUpgradesFrom(upgrades);
		}
		if(node.hasNonNull("upgrades_into") && node.get("upgrades_into").isArray())
		{
			UpgradeItem[] upgrades = mapper.treeToValue(node.get("upgrades_into"), UpgradeItem[].class);
			item.setUpgradesInto(upgrades);
		}
		if(node.hasNonNull("details") && type != null)
		{
			JsonNode details = node.get("details");
			switch(type)
			{
				case Armor: 
					Armor armor = mapper.treeToValue(details, Armor.class);
					item.setDetails(armor);
					break;
				case Weapon:
					Weapon weapon = mapper.treeToValue(details, Weapon.class);
					item.setDetails(weapon);
					break;
				case Bag:
					Bag bag = mapper.treeToValue(details, Bag.class);
					item.setDetails(bag);
					break;
				case BackItem:
					BackItem backItem = mapper.treeToValue(details, BackItem.class);
					item.setDetails(backItem);
					break;
				case Consumable:
					Consumable consumable = mapper.treeToValue(details, Consumable.class);
					item.setDetails(consumable);
					break;
				case Container:
					Container container = mapper.treeToValue(details, Container.class);
					item.setDetails(container);
					break;
				case GatheringTool:
					GatheringTool tool = mapper.treeToValue(details, GatheringTool.class);
					item.setDetails(tool);
					break;
				case Gizmo:
					Gizmo gizmo = mapper.treeToValue(details, Gizmo.class);
					item.setDetails(gizmo);
					break;
				case Miniatures:
					Miniature mini = mapper.treeToValue(details, Miniature.class);
					item.setDetails(mini);
					break;
				case SalvageKits:
					SalvageKits kits = mapper.treeToValue(details, SalvageKits.class);
					item.setDetails(kits);
					break;
				case Trinket:
					Trinket trinket = mapper.treeToValue(details, Trinket.class);
					item.setDetails(trinket);
					break;
				case UpgradeComponent:
					UpgradeComponent upgrade = mapper.treeToValue(details, UpgradeComponent.class);
					item.setDetails(upgrade);
					break;
				default:
					break;
			}			
		}
		return item;
	}

}
