package org.guild.wars.objects.items.upgrade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Infusion
{
	@JsonProperty("Enrichment")
	Enrichment ("Enrichment"),
	@JsonProperty("Infusion")
	Infusion ("Infusion"),
	@JsonProperty("Defense")
	Defense ("Defense"),
	@JsonProperty("Offense")
	Offense ("Offense"),
	@JsonProperty("Utility")
	Utility ("Utility"),
	@JsonProperty("Agony")
	Agony ("Agony");
	
	private final String value;
	
	Infusion(String value)
	{
		this.value = value;
	}
	
	public String getValue()
	{
		return this.value;
	}
	
}
