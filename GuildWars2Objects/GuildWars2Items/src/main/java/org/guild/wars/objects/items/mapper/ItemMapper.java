package org.guild.wars.objects.items.mapper;

import org.guild.wars.objects.items.Item;
import org.guild.wars.objects.items.ItemDeserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * 
 * @author Gab
 * An object mapper for deserializing an item.
 */

public class ItemMapper extends ObjectMapper
{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	public ItemMapper()
	{
		super();
		SimpleModule module = new SimpleModule();
		module.addDeserializer(Item.class, new ItemDeserializer());
		this.registerModule(module);
	}
}
