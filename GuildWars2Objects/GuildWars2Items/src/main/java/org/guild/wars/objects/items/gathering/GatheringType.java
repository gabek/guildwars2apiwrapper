package org.guild.wars.objects.items.gathering;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GatheringType
{
	@JsonProperty("Foraging")
	Foraging ("Foraging"),
	@JsonProperty("Logging")
	Logging ("Logging"),
	@JsonProperty("Mining")
	Mining ("Mining");
	
	private final String name;
	
	GatheringType(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
}
