package org.guild.wars.objects.items;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Miniature extends Details
{
	@JsonProperty(value="minipet_id", required=true)
	private int id;

	/**
	 * @return the id
	 */
	public int getId()
	{
		return id;
	}

	/**
	 * @param id the id to set
	 */
	protected void setId(int id)
	{
		this.id = id;
	}
	
	
}
