package org.guild.wars.objects.items;

import org.guild.wars.objects.items.common.InfixUpgrade;
import org.guild.wars.objects.items.common.InfusionSlots;
import org.guild.wars.objects.items.trinket.TrinketType;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Trinket extends Details
{
	@JsonProperty(value="type", required=true)
	private TrinketType type;
	
	@JsonProperty(value="infusion_slots", required=true)
	private InfusionSlots[] infusionSlots;
	
	@JsonProperty(value="infix_upgrade")
	private InfixUpgrade infixUpgrade;
	
	@JsonProperty("suffix_item_id")
	private int suffixItemId;
	
	@JsonProperty(value="secondary_suffix_item_id", required=true)
	private String secondarySuffixItemId;
	
	@JsonProperty("stat_choices")
	private int[] statChoices;

	/**
	 * @return the type
	 */
	public TrinketType getType()
	{
		return type;
	}

	/**
	 * @return the infusionSlots
	 */
	public InfusionSlots[] getInfusionSlots()
	{
		return infusionSlots;
	}

	/**
	 * @return the infixUpgrade
	 */
	public InfixUpgrade getInfixUpgrade()
	{
		return infixUpgrade;
	}

	/**
	 * @return the suffixItemId
	 */
	public int getSuffixItemId()
	{
		return suffixItemId;
	}

	/**
	 * @return the secondarySuffixItemId
	 */
	public String getSecondarySuffixItemId()
	{
		return secondarySuffixItemId;
	}

	/**
	 * @return the stat_choices
	 */
	public int[] getStatChoices()
	{
		return statChoices;
	}

	/**
	 * @param type the type to set
	 */
	protected void setType(TrinketType type)
	{
		this.type = type;
	}

	/**
	 * @param infusionSlots the infusionSlots to set
	 */
	protected void setInfusionSlots(InfusionSlots[] infusionSlots)
	{
		this.infusionSlots = infusionSlots;
	}

	/**
	 * @param infixUpgrade the infixUpgrade to set
	 */
	protected void setInfixUpgrade(InfixUpgrade infixUpgrade)
	{
		this.infixUpgrade = infixUpgrade;
	}

	/**
	 * @param suffixItemId the suffixItemId to set
	 */
	protected void setSuffixItemId(int suffixItemId)
	{
		this.suffixItemId = suffixItemId;
	}

	/**
	 * @param secondarySuffixItemId the secondarySuffixItemId to set
	 */
	protected void setSecondarySuffixItemId(String secondarySuffixItemId)
	{
		this.secondarySuffixItemId = secondarySuffixItemId;
	}

	/**
	 * @param statChoices the statChoices to set
	 */
	protected void setStatChoices(int[] statChoices)
	{
		this.statChoices = statChoices;
	}
}
