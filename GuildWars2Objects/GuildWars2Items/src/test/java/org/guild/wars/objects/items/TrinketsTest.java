package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.InfixInfusion;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.trinket.TrinketType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class TrinketsTest
{
	private String trinketsResource = "TrinketsJson";
	private ItemMapper mapper;
	
	public TrinketsTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testTrinkets1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(trinketsResource + "/test1.json");
		String trinket = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(trinket, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Trinket);
		expected.setName("Althea's Ashes");
		expected.setDescription("<c=@flavor>The legendary beauty who captured the heart of Prince Rurik, only to perish in the Searing of Ascalon.</c>");
		expected.setLevel(80);
		expected.setRarity(Rarity.Ascended);
		expected.setVendorValue(413);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NotUpgradeable, Flags.Unique, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrict = {};
		expected.setRestrictions(restrict);
		expected.setId(39233);
		expected.setChatLink("[&AgFBmQAA]");
		expected.setIcon("https://render.guildwars2.com/file/0E4C0C67CA06D97FD9E93C08BC600103D15AAF6A/543880.png");
		
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getGameTypes().length, expected.getGameTypes().length);
		for(int i=0; i< expected.getGameTypes().length; i++)
				assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(Trinket.class, result.getDetails().getClass());
		
		Trinket trinketResult = (Trinket) result.getDetails();
		
		TrinketType expectedType = TrinketType.Accessory;
		int expectedInfusionSize = 1;
		int expectedFlagSize = 1;
		InfixInfusion expectedFlag = InfixInfusion.Infusion;
		int expectedId = 584;
		String expectedSkill = "15742";
		String expectedDescription = "+32 Power\n+18 Ferocity\n+18 Precision";
		int expectedAttributeLength = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 110;
		AttributeName expectedAttribute2 = AttributeName.Precision;
		int expectedModifier2 = 74;
		AttributeName expectedAttribute3 = AttributeName.Ferocity;
		int expectedModifier3 = 74;
		String expectedSuffix = "";
		
		assertEquals(expectedType, trinketResult.getType());
		assertEquals(expectedInfusionSize, trinketResult.getInfusionSlots().length);
		assertEquals(expectedFlagSize, trinketResult.getInfusionSlots()[0].getFlags().length);
		assertEquals(expectedFlag, trinketResult.getInfusionSlots()[0].getFlags()[0]);
		assertEquals(expectedId, trinketResult.getInfixUpgrade().getId());
		assertEquals(expectedSkill, trinketResult.getInfixUpgrade().getBuff().getSkillId());
		assertEquals(expectedDescription, trinketResult.getInfixUpgrade().getBuff().getDescription());
		assertEquals(expectedAttributeLength, trinketResult.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, trinketResult.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, trinketResult.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, trinketResult.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, trinketResult.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, trinketResult.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, trinketResult.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, trinketResult.getSecondarySuffixItemId());
	}
	
	@Test
	public void testTrinkets2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(trinketsResource + "/test2.json");
		String trinket = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(trinket, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Trinket);
		expected.setName("Feedback Loop");
		expected.setLevel(80);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(396);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrict = {};
		expected.setRestrictions(restrict);
		expected.setId(72219);
		expected.setChatLink("[&AgEbGgEA]");
		expected.setIcon("https://render.guildwars2.com/file/D3340234F799E0DDF8FC48254B1EF16FAE69AB55/543924.png");
		
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getGameTypes().length, expected.getGameTypes().length);
		for(int i=0; i< expected.getGameTypes().length; i++)
				assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(Trinket.class, result.getDetails().getClass());
		
		Trinket trinketResult = (Trinket) result.getDetails();
		
		TrinketType expectedType = TrinketType.Ring;
		int expectedInfusionSize = 1;
		int expectedFlagSize = 1;
		InfixInfusion expectedFlag = InfixInfusion.Infusion;
		String expectedSuffix = "";
		int[] expectedStats = {161, 154, 158};
		
		assertEquals(expectedType, trinketResult.getType());
		assertEquals(expectedInfusionSize, trinketResult.getInfusionSlots().length);
		assertEquals(expectedFlagSize, trinketResult.getInfusionSlots()[0].getFlags().length);
		assertEquals(expectedFlag, trinketResult.getInfusionSlots()[0].getFlags()[0]);
		assertEquals(expectedSuffix, trinketResult.getSecondarySuffixItemId());
		assertEquals(expectedStats.length, trinketResult.getStatChoices().length);
		for(int i =0; i<expectedStats.length; i++)
			assertEquals(expectedStats[i],trinketResult.getStatChoices()[i]);
	}
	
	@Test
	public void testTrinkets3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(trinketsResource + "/test3.json");
		String trinket = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(trinket, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Trinket);
		expected.setName("Forgotten Seal");
		expected.setLevel(80);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(330);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrict = {};
		expected.setRestrictions(restrict);
		expected.setId(68532);
		expected.setChatLink("[&AgG0CwEA]");
		expected.setIcon("https://render.guildwars2.com/file/9C1A5FFEEFF1B94F04381BEDBDF5F91143786A5A/930899.png");
		
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getGameTypes().length, expected.getGameTypes().length);
		for(int i=0; i< expected.getGameTypes().length; i++)
				assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(Trinket.class, result.getDetails().getClass());
		
		Trinket trinketResult = (Trinket) result.getDetails();
		
		TrinketType expectedType = TrinketType.Accessory;
		int expectedInfusionSize = 0;
		String expectedSuffix = "";
		int[] expectedStats = {155, 161, 159, 157, 158, 160, 153, 605, 700, 616, 154, 156, 162, 686, 559, 754, 753 };
		int expectedSuffixId = 67914;
		
		assertEquals(expectedType, trinketResult.getType());
		assertEquals(expectedInfusionSize, trinketResult.getInfusionSlots().length);
		assertEquals(expectedSuffix, trinketResult.getSecondarySuffixItemId());
		assertEquals(expectedStats.length, trinketResult.getStatChoices().length);
		for(int i =0; i<expectedStats.length; i++)
			assertEquals(expectedStats[i],trinketResult.getStatChoices()[i]);
		assertEquals(expectedSuffixId, trinketResult.getSuffixItemId());
	}
}
