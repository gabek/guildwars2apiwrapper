package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Attribute;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.upgrade.Infusion;
import org.guild.wars.objects.items.upgrade.UpgradeFlag;
import org.guild.wars.objects.items.upgrade.UpgradeType;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class UpgradeTest
{
	private String upgradeResource = "UpgradeJson";
	private ItemMapper mapper;

	public UpgradeTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testUpgrade1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(upgradeResource + "/test1.json");
		String upgrade = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(upgrade, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgG0YAAA]");
		expected.setType(ItemType.UpgradeComponent);
		expected.setName("Superior Rune of the Ogre");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(24756);
		expected.setLevel(60);
		Restrictions[] restrict = {};
		Flags[] flags = {};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/136590B36FFC2053D135743B063405C9A54A9E6A/220716.png");
		expected.setVendorValue(65);
		expected.setRarity(Rarity.Exotic);
		expected.setDescription("<c=@abilitytype>Element: </c>Potence<br>Double-click to apply to a piece of armor.");
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(UpgradeComponent.class, result.getDetails().getClass());
		
		UpgradeComponent resultUpgrade = (UpgradeComponent) result.getDetails();
		
		UpgradeComponent expectedUpgrade = new UpgradeComponent();
		expectedUpgrade.setType(UpgradeType.Rune);
		UpgradeFlag[] expectedFlags = {UpgradeFlag.HeavyArmor, UpgradeFlag.LightArmor, UpgradeFlag.MediumArmor};
		Infusion[] expectedInfusions = {};
		String[] expectedBonuses = {"+25 Power", "+35 Ferocity", "+50 Power",
				"+65 Ferocity", "+100 Power", "+4% Damage; summon a rock dog while in combat. <c=@reminder>(Cooldown: 60 Seconds)</c>"};
		expectedUpgrade.setBonuses(expectedBonuses);
		expectedUpgrade.setFlags(expectedFlags);
		expectedUpgrade.setInfusions(expectedInfusions);
		int expectedId = 112;
		Attribute[] expectedAttributes = {};
		expectedUpgrade.setSuffix("of the Ogre");
		
		assertEquals(expectedUpgrade.getType(), resultUpgrade.getType());
		assertEquals(expectedId, resultUpgrade.getInfixUpgrade().getId());
		assertEquals(expectedUpgrade.getSuffix(), resultUpgrade.getSuffix());
		assertEquals(expectedUpgrade.getBonuses().length, resultUpgrade.getBonuses().length);
		for(int i = 0; i<expectedUpgrade.getBonuses().length; i++)
			assertEquals(expectedUpgrade.getBonuses()[i], resultUpgrade.getBonuses()[i]);
		assertEquals(expectedUpgrade.getFlags().length, resultUpgrade.getFlags().length);
		for(int i = 0; i<expectedUpgrade.getFlags().length; i++)
			assertEquals(expectedUpgrade.getFlags()[i], resultUpgrade.getFlags()[i]);		
		assertEquals(expectedUpgrade.getInfusions().length, resultUpgrade.getInfusions().length);
		for(int i = 0; i<expectedUpgrade.getInfusions().length; i++)
			assertEquals(expectedUpgrade.getInfusions()[i], resultUpgrade.getInfusions()[i]);
		assertEquals(expectedAttributes.length, resultUpgrade.getInfixUpgrade().getAttributes().length);
	}
	
	@Test
	public void testUpgrade2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(upgradeResource + "/test2.json");
		String upgrade = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(upgrade, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgGPrwAA]");
		expected.setType(ItemType.UpgradeComponent);
		expected.setName("Major Sigil of Bursting");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(44943);
		expected.setLevel(39);
		Restrictions[] restrict = {};
		Flags[] flags = {};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/23BB0880D81AE1C4B60D0C0297F9E1111D0A7352/619702.png");
		expected.setVendorValue(108);
		expected.setRarity(Rarity.Rare);
		expected.setDescription("<c=@abilitytype>Element: </c>Enhancement<br>Double-click to apply to a weapon.");
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(UpgradeComponent.class, result.getDetails().getClass());
		
		UpgradeComponent resultUpgrade = (UpgradeComponent) result.getDetails();
		
		UpgradeComponent expectedUpgrade = new UpgradeComponent();
		expectedUpgrade.setType(UpgradeType.Sigil);
		UpgradeFlag[] expectedFlags = {UpgradeFlag.ShortBow, UpgradeFlag.Dagger, UpgradeFlag.Focus,
						UpgradeFlag.Greatsword, UpgradeFlag.Hammer, UpgradeFlag.Harpoon, UpgradeFlag.Mace,
						UpgradeFlag.Pistol, UpgradeFlag.Rifle, UpgradeFlag.Scepter, UpgradeFlag.Shield,
						UpgradeFlag.Speargun, UpgradeFlag.Axe, UpgradeFlag.Staff, UpgradeFlag.Sword,
						UpgradeFlag.Torch, UpgradeFlag.Trident, UpgradeFlag.Warhorn, UpgradeFlag.LongBow};
		Infusion[] expectedInfusions = {};
		expectedUpgrade.setFlags(expectedFlags);
		expectedUpgrade.setInfusions(expectedInfusions);
		int expectedId = 747;
		String expectedSkill = "20473";
		String expectedDescription = "+3% Condition Damage";
		Attribute[] expectedAttributes = {};
		expectedUpgrade.setSuffix("of Bursting");
		
		assertEquals(expectedUpgrade.getType(), resultUpgrade.getType());
		assertEquals(expectedId, resultUpgrade.getInfixUpgrade().getId());
		assertEquals(expectedUpgrade.getSuffix(), resultUpgrade.getSuffix());
		assertEquals(expectedUpgrade.getFlags().length, resultUpgrade.getFlags().length);
		for(int i = 0; i<expectedUpgrade.getFlags().length; i++)
			assertEquals(expectedUpgrade.getFlags()[i], resultUpgrade.getFlags()[i]);		
		assertEquals(expectedUpgrade.getInfusions().length, resultUpgrade.getInfusions().length);
		for(int i = 0; i<expectedUpgrade.getInfusions().length; i++)
			assertEquals(expectedUpgrade.getInfusions()[i], resultUpgrade.getInfusions()[i]);
		assertEquals(expectedAttributes.length, resultUpgrade.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedSkill, resultUpgrade.getInfixUpgrade().getBuff().getSkillId());
		assertEquals(expectedDescription, resultUpgrade.getInfixUpgrade().getBuff().getDescription());
	}
	
	@Test
	public void testUpgrade3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(upgradeResource + "/test3.json");
		String upgrade = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(upgrade, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgHEmgAA]");
		expected.setType(ItemType.UpgradeComponent);
		expected.setName("Mighty +5 Agony Infusion");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(39620);
		expected.setLevel(0);
		Restrictions[] restrict = {};
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/1B18A669E1900107755B12ABAA7DD2071E974C53/511835.png");
		expected.setVendorValue(8);
		expected.setRarity(Rarity.Fine);
		expected.setDescription("Double-click to apply to an unused infusion slot.");
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(UpgradeComponent.class, result.getDetails().getClass());
		
		UpgradeComponent resultUpgrade = (UpgradeComponent) result.getDetails();
		
		UpgradeComponent expectedUpgrade = new UpgradeComponent();
		expectedUpgrade.setType(UpgradeType.Default);
		UpgradeFlag[] expectedFlags = {UpgradeFlag.ShortBow, UpgradeFlag.HeavyArmor, UpgradeFlag.LightArmor,
						UpgradeFlag.Dagger, UpgradeFlag.MediumArmor, UpgradeFlag.Focus, 
						UpgradeFlag.Greatsword, UpgradeFlag.Hammer, UpgradeFlag.Trinket, UpgradeFlag.Harpoon, UpgradeFlag.Mace,
						UpgradeFlag.Pistol, UpgradeFlag.Rifle, UpgradeFlag.Scepter, UpgradeFlag.Shield,
						UpgradeFlag.Speargun, UpgradeFlag.Axe, UpgradeFlag.Staff, UpgradeFlag.Sword,
						UpgradeFlag.Torch, UpgradeFlag.Trident, UpgradeFlag.Warhorn, UpgradeFlag.LongBow};
		Infusion[] expectedInfusions = {Infusion.Infusion};
		expectedUpgrade.setFlags(expectedFlags);
		expectedUpgrade.setInfusions(expectedInfusions);
		int expectedId = 576;
		String expectedSkill = "15735";
		String expectedDescription = "+5 Power\n+5 Agony Resistance";
		int expectedAttributeSize = 2;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 5;
		AttributeName expectedAttribute2 = AttributeName.AgonyResistance;
		int expectedModifier2 = 5;
		expectedUpgrade.setSuffix("");
		
		assertEquals(expectedUpgrade.getType(), resultUpgrade.getType());
		assertEquals(expectedId, resultUpgrade.getInfixUpgrade().getId());
		assertEquals(expectedUpgrade.getSuffix(), resultUpgrade.getSuffix());
		assertEquals(expectedUpgrade.getFlags().length, resultUpgrade.getFlags().length);
		for(int i = 0; i<expectedUpgrade.getFlags().length; i++)
			assertEquals(expectedUpgrade.getFlags()[i], resultUpgrade.getFlags()[i]);		
		assertEquals(expectedUpgrade.getInfusions().length, resultUpgrade.getInfusions().length);
		for(int i = 0; i<expectedUpgrade.getInfusions().length; i++)
			assertEquals(expectedUpgrade.getInfusions()[i], resultUpgrade.getInfusions()[i]);
		assertEquals(expectedAttributeSize, resultUpgrade.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultUpgrade.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultUpgrade.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultUpgrade.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultUpgrade.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedSkill, resultUpgrade.getInfixUpgrade().getBuff().getSkillId());
		assertEquals(expectedDescription, resultUpgrade.getInfixUpgrade().getBuff().getDescription());
	}
}
