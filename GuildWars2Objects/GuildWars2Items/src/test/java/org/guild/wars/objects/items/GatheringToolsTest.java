package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.mapper.ItemMapper;

import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.gathering.GatheringType;

import org.junit.jupiter.api.Test;

public class GatheringToolsTest
{
	private String gatheringResource = "GatheringToolJson";
	private ItemMapper mapper;
	
	public GatheringToolsTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testGatheringTools1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gatheringResource + "/test1.json");
		String gatheringJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gatheringJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Unbreakable Harvesting Sickle");
		expected.setDescription("Extremely efficient at gathering resources despite its unremarkable appearance, this Black Lion tool will never break or require replacing.");
		expected.setType(ItemType.GatheringTool);
		expected.setLevel(0);
		expected.setRarity(Rarity.Rare);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(78996);
		expected.setChatLink("[&AgGUNAEA]");
		expected.setIcon("https://render.guildwars2.com/file/DC910B7920BDA702A5F7FEAE4A4BAE32EF0CC0A5/1459298.png");
		Class<GatheringTool> expectedClass = GatheringTool.class;
		GatheringType expectedType = GatheringType.Foraging;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		GatheringTool resultTool = (GatheringTool) result.getDetails();
		
		assertEquals(expectedType, resultTool.getType());
	}
	
	@Test
	public void testGatheringTools2() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gatheringResource + "/test2.json");
		String gatheringJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gatheringJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Orichalcum Mining Pick");
		expected.setDescription("Used to mine all metals.");
		expected.setType(ItemType.GatheringTool);
		expected.setLevel(60);
		expected.setRarity(Rarity.Fine);
		expected.setVendorValue(50);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell,Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(23001);
		expected.setChatLink("[&AgHZWQAA]");
		expected.setIcon("https://render.guildwars2.com/file/3CD995B576C8CB39B02D4826EA04690701755ECB/66254.png");
		Class<GatheringTool> expectedClass = GatheringTool.class;
		GatheringType expectedType = GatheringType.Mining;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		GatheringTool resultTool = (GatheringTool) result.getDetails();
		
		assertEquals(expectedType, resultTool.getType());
	}
	
	@Test
	public void testGatheringTools3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gatheringResource + "/test3.json");
		String gatheringJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gatheringJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Copper Logging Axe");
		expected.setDescription("Used to chop down Aspen Saplings, Kertch Saplings, and Ekku saplings.");
		expected.setType(ItemType.GatheringTool);
		expected.setLevel(0);
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(3);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell,Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(23030);
		expected.setChatLink("[&AgH2WQAA]");
		expected.setIcon("https://render.guildwars2.com/file/201CC6E60448CB4BA8AD919C74904EE1F9AEC54F/66597.png");
		Class<GatheringTool> expectedClass = GatheringTool.class;
		GatheringType expectedType = GatheringType.Logging;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		GatheringTool resultTool = (GatheringTool) result.getDetails();
		
		assertEquals(expectedType, resultTool.getType());
	}
}
