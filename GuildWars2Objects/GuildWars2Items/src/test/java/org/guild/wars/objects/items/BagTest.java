package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class BagTest
{
	private String bagResource = "BagJson";
	private ObjectMapper mapper;
	
	public BagTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testBag1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(bagResource + "/test1.json");
		String bag = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(bag, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Bag);
		expected.setName("18 Slot Thick Leather Pack");
		expected.setLevel(0);
		expected.setDescription("18 Slots");
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(27);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(9576);
		expected.setChatLink("[&AgFoJQAA]");
		expected.setIcon("https://render.guildwars2.com/file/545E084015DC05A555170BB3D063193A346E1573/222432.png");
		Class<Bag> expectedClass = Bag.class;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Bag resultBag = (Bag) result.getDetails();
		boolean expectedInvisible = false;
		int expectedSize = 18;
		
		assertEquals(expectedInvisible, resultBag.isInvisible());
		assertEquals(expectedSize, resultBag.getSize());
	}
	
	@Test
	public void testBag2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(bagResource + "/test2.json");
		String bag = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(bag, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Bag);
		expected.setName("Starter Backpack");
		expected.setLevel(0);
		expected.setDescription("A 20 slot bag for beginning characters.");
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(11);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.NoSell, Flags.SoulbindOnAcquire, Flags.SoulBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(8932);
		expected.setChatLink("[&AgHkIgAA]");
		expected.setIcon("https://render.guildwars2.com/file/80E36806385691D4C0910817EF2A6C2006AEE353/61755.png");
		Class<Bag> expectedClass = Bag.class;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Bag resultBag = (Bag) result.getDetails();
		boolean expectedInvisible = false;
		int expectedSize = 20;
		
		assertEquals(expectedInvisible, resultBag.isInvisible());
		assertEquals(expectedSize, resultBag.getSize());
	}
	
	@Test
	public void testBag3() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(bagResource + "/test3.json");
		String bag = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(bag, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Bag);
		expected.setName("18 Slot Invisible Bag");
		expected.setLevel(0);
		expected.setDescription("18 Slots. Items in this bag will never appear in a sell-to-vendor list and will not move when inventory is sorted.");
		expected.setRarity(Rarity.Fine);
		expected.setVendorValue(22);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(9569);
		expected.setChatLink("[&AgFhJQAA]");
		expected.setIcon("https://render.guildwars2.com/file/D65F01A57E4BBABC5D465EFD035A5A5518CA9900/433575.png");
		Class<Bag> expectedClass = Bag.class;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Bag resultBag = (Bag) result.getDetails();
		boolean expectedInvisible = true;
		int expectedSize = 18;
		
		assertEquals(expectedInvisible, resultBag.isInvisible());
		assertEquals(expectedSize, resultBag.getSize());
	}
}
