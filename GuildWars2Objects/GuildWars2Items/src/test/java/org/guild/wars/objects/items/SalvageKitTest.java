package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class SalvageKitTest
{
	private String kitResource = "SalvageJson";
	private ItemMapper mapper;
	
	public SalvageKitTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testSalvageKit1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(kitResource + "/test1.json");
		String salvageKit = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(salvageKit, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.SalvageKits);
		expected.setName("Mystic Salvage Kit");
		expected.setLevel(0);
		expected.setDescription("Double-click to salvage crafting materials from an item in your inventory. 25% chance of rarer materials. 80% chance of salvaging upgrades.");
		expected.setRarity(Rarity.Rare);
		expected.setVendorValue(192);
		GameTypes[] gameModes = { GameTypes.PvP, GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(23045);
		expected.setChatLink("[&AgEFWgAA]");
		expected.setIcon("https://render.guildwars2.com/file/9F09ACD431CE4906631E271E650ABB0454F90705/63123.png");

		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(SalvageKits.class, result.getDetails().getClass());
		
		SalvageKits resultKit = (SalvageKits) result.getDetails();
		
		SalvageKits expectedKit = new SalvageKits();
		expectedKit.setCharges(250);
		expectedKit.setType("Salvage");
		
		assertEquals(expectedKit.getCharges(), resultKit.getCharges());
		assertEquals(expectedKit.getType(), resultKit.getType());
	}
	
	@Test
	public void testSalvageKit2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(kitResource + "/test2.json");
		String salvageKit = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(salvageKit, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.SalvageKits);
		expected.setName("Ascended Salvage Kit");
		expected.setLevel(0);
		expected.setDescription("Double-click to salvage crafting materials from an ascended item in your inventory. Stabilizing Matrices can be salvaged from rings. Dark Energy and other items can be salvaged from weapons, armor, and other trinkets.");
		expected.setRarity(Rarity.Ascended);
		expected.setVendorValue(320);
		GameTypes[] gameModes = { GameTypes.PvP, GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(73481);
		expected.setChatLink("[&AgEJHwEA]");
		expected.setIcon("https://render.guildwars2.com/file/061C07D229FF2695099828F551F02EB8B1B82EAE/1200207.png");

		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(SalvageKits.class, result.getDetails().getClass());
		
		SalvageKits resultKit = (SalvageKits) result.getDetails();
		
		SalvageKits expectedKit = new SalvageKits();
		expectedKit.setCharges(20);
		expectedKit.setType("Salvage");
		
		assertEquals(expectedKit.getCharges(), resultKit.getCharges());
		assertEquals(expectedKit.getType(), resultKit.getType());
	}
	
	@Test
	public void testSalvageKit3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(kitResource + "/test3.json");
		String salvageKit = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(salvageKit, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.SalvageKits);
		expected.setName("Copper-Fed Salvage-o-Matic");
		expected.setLevel(0);
		expected.setDescription("Double-click to salvage crafting materials from an item in your inventory. 10% chance of rarer materials. 20% chance of salvaging upgrades. Requires 3 copper per use.");
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(0);
		GameTypes[] gameModes = { GameTypes.PvP, GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(44602);
		expected.setChatLink("[&AgE6rgAA]");
		expected.setIcon("https://render.guildwars2.com/file/CC2004000FFDFCEF346AAE296FD0E858C0990548/619581.png");

		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(SalvageKits.class, result.getDetails().getClass());
		
		SalvageKits resultKit = (SalvageKits) result.getDetails();
		
		SalvageKits expectedKit = new SalvageKits();
		expectedKit.setCharges(1);
		expectedKit.setType("Salvage");
		
		assertEquals(expectedKit.getCharges(), resultKit.getCharges());
		assertEquals(expectedKit.getType(), resultKit.getType());
	}
}
