package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.consumables.ConsumableType;
import org.guild.wars.objects.items.consumables.Unlock;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;

public class ConsumableTest
{
	private String consumableResource = "ConsumableJson";
	private ItemMapper mapper;
		
	public ConsumableTest()
	{
		mapper = new ItemMapper();
	}
		
	@Test
	public void testConsumable1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(consumableResource + "/test1.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Consumable);
		expected.setName("Tome of Knowledge");
		expected.setDescription("Grants one character level if below level 80 or one spirit shard if level 80.\nOnly usable in PvE and WvW.");
		expected.setLevel(0);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(43766);
		expected.setChatLink("[&AgH2qgAA]");
		expected.setIcon("https://render.guildwars2.com/file/1932B731E2F70F2F1E3D453A4B7C26B24CF647C0/603246.png");
		Class<Consumable> expectedClass = Consumable.class;
		
		ConsumableType expectedType = ConsumableType.Generic;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Consumable resultsConsumable = (Consumable) result.getDetails();
		
		assertEquals(expectedType, resultsConsumable.getType());
	}
	
	@Test
	public void testConsumable2() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(consumableResource + "/test2.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Consumable);
		expected.setName("Plate of Truffle Steak");
		expected.setLevel(80);
		expected.setRarity(Rarity.Fine);
		expected.setVendorValue(33);
		GameTypes[] gameModes = {GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.NoSell};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(12467);
		expected.setChatLink("[&AgGzMAAA]");
		expected.setIcon("https://render.guildwars2.com/file/EB78371F9DADA4A130EDBD0D192D320402370F47/433656.png");
		Class<Consumable> expectedClass = Consumable.class;
		
		ConsumableType expectedType = ConsumableType.Food;
		Long expectedDuration = 1800000l;
		int expectedCount = 1;
		String expectedName = "Nourishment";
		String expectedIcon = "https://render.guildwars2.com/file/779D3F0ABE5B46C09CFC57374DA8CC3A495F291C/436367.png";
		String expectedDescription = "+100 Power\n+70 Precision\n+10% Experience from Kills";
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Consumable resultsConsumable = (Consumable) result.getDetails();
		
		assertEquals(expectedType, resultsConsumable.getType());
		assertEquals(expectedName, resultsConsumable.getName());
		assertEquals(expectedIcon, resultsConsumable.getIcon());
		assertEquals(expectedDuration, resultsConsumable.getDuration());
		assertEquals(expectedCount, resultsConsumable.getApplyCount());
		assertEquals(expectedDescription, resultsConsumable.getDescription());
	}
	
	@Test
	public void testConsumable3() throws IOException
	{


		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(consumableResource + "/test3.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Consumable);
		expected.setName("Recipe: Angchu Cavalier Inscription");
		expected.setDescription("A recipe to make an ascended inscription for crafting weapons with Cavalier (+Toughness, +Power, and +Ferocity) stats.");
		expected.setLevel(0);
		expected.setRarity(Rarity.Ascended);
		expected.setVendorValue(45);
		GameTypes[] gameModes = {GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(45593);
		expected.setChatLink("[&AgEZsgAA]");
		expected.setIcon("https://render.guildwars2.com/file/EE1D049599C275DE570FF9FF55051639DFFF7339/849337.png");
		Class<Consumable> expectedClass = Consumable.class;
		
		ConsumableType expectedType = ConsumableType.Unlock;
		Unlock expectedUnlock = Unlock.CraftingRecipe;
		int expectedId = 7335;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Consumable resultsConsumable = (Consumable) result.getDetails();

		assertEquals(expectedType, resultsConsumable.getType());
		assertEquals(expectedUnlock, resultsConsumable.getUnlock());
		assertEquals(expectedId, resultsConsumable.getRecipeId());
	}
}
