package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.InfixInfusion;
import org.guild.wars.objects.items.common.InfusionSlots;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class BackItemTest
{
	private String backResource = "BackItemJson";
	private ItemMapper mapper;
	
	public BackItemTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testBackItem1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(backResource + "/test1.json");
		String backItem = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(backItem, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.BackItem);
		expected.setName("Sclerite Karka Shell");
		expected.setLevel(80);
		expected.setDescription("<c=@flavor>The karka have mysterious biological properties.</c>\nCan be upgraded in the Mystic Forge.");
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(264);
		expected.setDefaultSkin(2354);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(66206);
		expected.setChatLink("[&AgGeAgEA]");
		expected.setIcon("https://render.guildwars2.com/file/0333A5CC80A85FD02F3F049AAEB9DF780DB8C8F2/582340.png");

		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(BackItem.class, result.getDetails().getClass());
		
		BackItem resultBack = (BackItem) result.getDetails();
		
		InfusionSlots[] expectedSlots = {};
		int expectedInfixId = 700;
		int expectedAttributeSize = 3;
		AttributeName expectedAttribute1 = AttributeName.Toughness;
		int expectedModifier1 = 30;
		AttributeName expectedAttribute2 = AttributeName.HealingPower;
		int expectedModifier2 = 21;
		AttributeName expectedAttribute3 = AttributeName.ConditionDamage;
		int expectedModifier3 = 21;
		String expectedSuffix = "";
		
		assertEquals(expectedSlots.length, resultBack.getInfusionSlots().length);
		assertEquals(expectedInfixId, resultBack.getInfixUpgrade().getId());
		assertEquals(expectedAttributeSize, resultBack.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultBack.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultBack.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultBack.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultBack.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultBack.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultBack.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultBack.getSecondarySuffixItemId());
	}
	
	@Test
	public void testBackItem2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(backResource + "/test2.json");
		String backItem = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(backItem, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.BackItem);
		expected.setName("Beta Fractal Capacitor (Infused)");
		expected.setLevel(80);
		expected.setDescription("");
		expected.setRarity(Rarity.Ascended);
		expected.setVendorValue(330);
		expected.setDefaultSkin(2378);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NoSell, Flags.NotUpgradeable, Flags.Unique, Flags.AccountBindOnUse, Flags.Infused};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(49381);
		expected.setChatLink("[&AgHlwAAA]");
		expected.setIcon("https://render.guildwars2.com/file/A4970276A9E49524A944C3CD66F3D7E237485CFF/511806.png");
		int expectedUpgradesSize = 1;
		String expectedUpgrade = "Infusion";
		int expectedItem = 37029;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedUpgradesSize, result.getUpgradesFrom().length);
		assertEquals(expectedUpgrade, result.getUpgradesFrom()[0].getUpgrade());
		assertEquals(expectedItem, result.getUpgradesFrom()[0].getItemId());
		assertEquals(BackItem.class, result.getDetails().getClass());
		
		BackItem resultBack = (BackItem) result.getDetails();
		
		int expectedSlotNumber = 2;
		InfixInfusion expectedInfusion = InfixInfusion.Infusion;
		String expectedSkillId = "15755";
		String expectedDescription = "+32 Power\n+18 Ferocity\n+18 Precision\n+5 Agony Resistance";
		
		int expectedInfixId = 599;
		int expectedAttributeSize = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 63;
		AttributeName expectedAttribute2 = AttributeName.Precision;
		int expectedModifier2 = 40;
		AttributeName expectedAttribute3 = AttributeName.Ferocity;
		int expectedModifier3 = 40;
		String expectedSuffix = "";
		
		assertEquals(expectedSlotNumber, resultBack.getInfusionSlots().length);
		assertEquals(expectedInfusion, resultBack.getInfusionSlots()[0].getFlags()[0]);
		assertEquals(expectedInfusion, resultBack.getInfusionSlots()[1].getFlags()[0]);
		assertEquals(expectedInfixId, resultBack.getInfixUpgrade().getId());
		assertEquals(expectedSkillId, resultBack.getInfixUpgrade().getBuff().getSkillId());
		assertEquals(expectedDescription, resultBack.getInfixUpgrade().getBuff().getDescription());
		assertEquals(expectedAttributeSize, resultBack.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultBack.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultBack.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultBack.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultBack.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultBack.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultBack.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultBack.getSecondarySuffixItemId());
	}
	
	@Test
	public void testBackItem3() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(backResource + "/test3.json");
		String backItem = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(backItem, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.BackItem);
		expected.setName("Soldier's Spineguard");
		expected.setLevel(80);
		expected.setDescription(null);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(6000);
		expected.setDefaultSkin(2329);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.SoulBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(39151);
		expected.setChatLink("[&AgHvmAAA]");
		expected.setIcon("https://render.guildwars2.com/file/03B65C435B15EB2C10E04F3454B03718AAF3AE90/61004.png");
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(BackItem.class, result.getDetails().getClass());
		
		BackItem resultBack = (BackItem) result.getDetails();
		
		int expectedSlotNumber = 0;
		
		int expectedInfixId = 162;
		int expectedAttributeSize = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 30;
		AttributeName expectedAttribute2 = AttributeName.Toughness;
		int expectedModifier2 = 21;
		AttributeName expectedAttribute3 = AttributeName.Vitality;
		int expectedModifier3 = 21;
		String expectedSuffix = "";
		
		assertEquals(expectedSlotNumber, resultBack.getInfusionSlots().length);
		assertEquals(expectedInfixId, resultBack.getInfixUpgrade().getId());
		assertEquals(expectedAttributeSize, resultBack.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultBack.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultBack.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultBack.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultBack.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultBack.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultBack.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultBack.getSecondarySuffixItemId());
	}
}
