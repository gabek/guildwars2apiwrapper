package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.armor.ArmorType;
import org.guild.wars.objects.items.armor.ArmorWeight;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.InfusionSlots;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * 
 * @author Gab
 * TODO: finish first test.
 */

public class ArmorTest
{
	private String armorResource = "ArmorJson";
	private ItemMapper mapper;

	public ArmorTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testArmor1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(armorResource + "/test1.json");
		String armor = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(armor, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgEdAQAA]");
		expected.setType(ItemType.Armor);
		expected.setName("Carrion Cabalist Coat");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(285);
		expected.setLevel(80);
		Restrictions[] restrict = {};
		Flags[] flags = {};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/73CEB0F9515E0627321A15DA1CA4A6F3F51A4CE6/61019.png");
		expected.setVendorValue(99);
		expected.setDefaultSkin(51);
		expected.setRarity(Rarity.Fine);
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(Armor.class, result.getDetails().getClass());
		
		Armor resultArmor = (Armor) result.getDetails();
		
		ArmorType expectedArmorType = ArmorType.Chest;
		int expectedDefense = 238;
		ArmorWeight expectedWeight = ArmorWeight.Light;
		InfusionSlots[] expectedSlots = {};
		int expectedId = 160;
		int expectedAttributeLength = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 73;
		AttributeName expectedAttribute2 = AttributeName.Vitality;
		int expectedModifier2 = 73;
		AttributeName expectedAttribute3 = AttributeName.ConditionDamage;
		int expectedModifier3 = 102;
		String expectedSuffix = "";
		
		assertEquals(expectedArmorType,resultArmor.getArmorType());
		assertEquals(expectedDefense, resultArmor.getDefense());
		assertEquals(expectedWeight, resultArmor.getWeight());
		assertEquals(expectedSlots.length, resultArmor.getInfusionSlots().length);
		assertEquals(expectedId, resultArmor.getInfixUpgrade().getId());
		assertEquals(expectedAttributeLength, resultArmor.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultArmor.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultArmor.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultArmor.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultArmor.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultArmor.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultArmor.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultArmor.getSecondarySuffixItemId());
	}
	
	@Test
	public void testArmor2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(armorResource + "/test2.json");
		String armor = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(armor, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgG8QwAA]");
		expected.setType(ItemType.Armor);
		expected.setName("Flame Legion Shoulders");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(17340);
		expected.setLevel(80);
		Restrictions[] restrict = {};
		Flags[] flags = {Flags.AccountBound, Flags.NoSell, Flags.SoulBindOnUse};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/5FF158730A271820FE3CBDB4067717989070273F/61591.png");
		expected.setVendorValue(291);
		expected.setDefaultSkin(733);
		expected.setRarity(Rarity.Exotic);
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i=0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(Armor.class, result.getDetails().getClass());
		
		Armor resultArmor = (Armor) result.getDetails();
		
		ArmorType expectedArmorType = ArmorType.Shoulders;
		int expectedDefense = 97;
		ArmorWeight expectedWeight = ArmorWeight.Medium;
		InfusionSlots[] expectedSlots = {};
		int expectedId = 161;
		int expectedAttributeLength = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 45;
		AttributeName expectedAttribute2 = AttributeName.Precision;
		int expectedModifier2 = 32;
		AttributeName expectedAttribute3 = AttributeName.Ferocity;
		int expectedModifier3 = 32;
		String expectedSuffix = "";
		int expectedSuffixId = 24854;
		
		assertEquals(expectedArmorType,resultArmor.getArmorType());
		assertEquals(expectedDefense, resultArmor.getDefense());
		assertEquals(expectedWeight, resultArmor.getWeight());
		assertEquals(expectedSlots.length, resultArmor.getInfusionSlots().length);
		assertEquals(expectedId, resultArmor.getInfixUpgrade().getId());
		assertEquals(expectedAttributeLength, resultArmor.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultArmor.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultArmor.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultArmor.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultArmor.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultArmor.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultArmor.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultArmor.getSecondarySuffixItemId());
		assertEquals(expectedSuffixId, resultArmor.getSuffixItemId());
	}
	
	@Test
	public void testArmor3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(armorResource + "/test3.json");
		String armor = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(armor, Item.class);
		Item expected = new Item();
		
		expected.setChatLink("[&AgEnDgAA]");
		expected.setType(ItemType.Armor);
		expected.setName("Carrion Electromagnetic Coat");
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		expected.setId(3623);
		expected.setLevel(80);
		Restrictions[] restrict = { Restrictions.Asura};
		Flags[] flags = {Flags.SoulbindOnAcquire, Flags.SoulBindOnUse};
		expected.setRestrictions(restrict);
		expected.setFlags(flags);
		expected.setIcon("https://render.guildwars2.com/file/980F20B3C2620EC0C2343D4D33F17FD6B698FC6B/511732.png");
		expected.setVendorValue(37500);
		expected.setDefaultSkin(313);
		expected.setRarity(Rarity.Rare);
		
		assertEquals(expected.getChatLink(),result.getChatLink());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < gameModes.length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		for(int i = 0; i< expected.getRestrictions().length; i++)
			assertEquals(expected.getRestrictions()[i], result.getRestrictions()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i=0; i< flags.length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getDefaultSkin(), result.getDefaultSkin());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(Armor.class, result.getDetails().getClass());
		
		Armor resultArmor = (Armor) result.getDetails();
		
		ArmorType expectedArmorType = ArmorType.Chest;
		int expectedDefense = 321;
		ArmorWeight expectedWeight = ArmorWeight.Heavy;
		InfusionSlots[] expectedSlots = {};
		int expectedId = 160;
		int expectedAttributeLength = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 85;
		AttributeName expectedAttribute2 = AttributeName.Vitality;
		int expectedModifier2 = 85;
		AttributeName expectedAttribute3 = AttributeName.ConditionDamage;
		int expectedModifier3 = 119;
		String expectedSuffix = "";
		
		assertEquals(expectedArmorType,resultArmor.getArmorType());
		assertEquals(expectedDefense, resultArmor.getDefense());
		assertEquals(expectedWeight, resultArmor.getWeight());
		assertEquals(expectedSlots.length, resultArmor.getInfusionSlots().length);
		assertEquals(expectedId, resultArmor.getInfixUpgrade().getId());
		assertEquals(expectedAttributeLength, resultArmor.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultArmor.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultArmor.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultArmor.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultArmor.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultArmor.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultArmor.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffix, resultArmor.getSecondarySuffixItemId());
	}

}
