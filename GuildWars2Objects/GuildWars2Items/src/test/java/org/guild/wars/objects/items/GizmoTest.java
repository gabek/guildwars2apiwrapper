package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.gizmo.GizmoType;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.junit.jupiter.api.Test;


public class GizmoTest
{
	private String gizmoResource = "GizmoJson";
	private ItemMapper mapper;
	
	public GizmoTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testGizmo1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gizmoResource + "/test1.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Gizmo);
		expected.setName("101 Things the Mordant Crescent Doesn't Want You to Know!");
		expected.setDescription("Checked out by 5-Finger Fareem. Last seen being arrested for subversion.<br><br>Double-click from inventory to read.");
		expected.setLevel(0);
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(85901);
		expected.setChatLink("[&AgGNTwEA]");
		expected.setIcon("https://render.guildwars2.com/file/F70D98EB32C709C9ABC8702719A84D754A5C01BB/1894641.png");
		Class<Gizmo> expectedClass = Gizmo.class;
		
		GizmoType expectedType = GizmoType.Default;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Gizmo resultsGizmo = (Gizmo) result.getDetails();
		
		assertEquals(expectedType, resultsGizmo.getType());
	}
	
	@Test
	public void testGizmo2() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gizmoResource + "/test2.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Gizmo);
		expected.setName("Black Lion Chest Key");
		expected.setDescription("This key will unlock one Black Lion Chest containing random Gem Store merchandise, including some rare items not sold separately.\n\nBlack Lion Chests can be found randomly on enemies or bought from the trading post.");
		expected.setLevel(0);
		expected.setRarity(Rarity.Fine);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(19980);
		expected.setChatLink("[&AgEMTgAA]");
		expected.setIcon("https://render.guildwars2.com/file/207BDD31BC494A07A0A1691705079100066D3F2F/414998.png");
		Class<Gizmo> expectedClass = Gizmo.class;
		
		GizmoType expectedType = GizmoType.ContainerKey;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Gizmo resultsGizmo = (Gizmo) result.getDetails();
		
		assertEquals(expectedType, resultsGizmo.getType());
	}
	
	@Test
	public void testGizmo3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(gizmoResource + "/test3.json");
		String gizmoJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(gizmoJson, Item.class);
		
		Item expected = new Item();
		expected.setType(ItemType.Gizmo);
		expected.setName("Banker Golem (2 weeks)");
		expected.setDescription("Double-click to access your account bank from anywhere. Reusable.");
		expected.setLevel(0);
		expected.setRarity(Rarity.Masterwork);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(36171);
		expected.setChatLink("[&AgFLjQAA]");
		expected.setIcon("https://render.guildwars2.com/file/EE415AA4CF929ED93D5CC168C80F33D453D24EAF/66598.png");
		Class<Gizmo> expectedClass = Gizmo.class;
		
		GizmoType expectedType = GizmoType.RentableContractNpc;
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Gizmo resultsGizmo = (Gizmo) result.getDetails();
		
		assertEquals(expectedType, resultsGizmo.getType());
	}

}
