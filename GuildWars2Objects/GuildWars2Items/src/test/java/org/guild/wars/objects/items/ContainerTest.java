package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.io.IOUtils;

import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.container.ContainerType;

import org.junit.jupiter.api.Test;


public class ContainerTest
{
	private String containerResource = "ContainerJson";
	private ItemMapper mapper;
		
	public ContainerTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testContainer1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test1.json");
		String containerJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(containerJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Light Loot Bag");
		expected.setDescription("Double-click to open.");
		expected.setType(ItemType.Container);
		expected.setLevel(0);
		expected.setRarity(Rarity.Basic);
		expected.setVendorValue(15);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(8915);
		expected.setChatLink("[&AgHTIgAA]");
		expected.setIcon("https://render.guildwars2.com/file/EBC5CEC199D1E51B02756A1C796A65E9D24F04B5/63171.png");
		ContainerType expectedType = ContainerType.Default;
		Class<Container> expectedClass = Container.class;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i<result.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Container resultContainer = (Container) result.getDetails();
		assertEquals(expectedType, resultContainer.getType());
	}
	
	@Test
	public void testContainer2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test2.json");
		String containerJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(containerJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Black Lion Chest");
		expected.setDescription("Contains a guaranteed item, a redeemable Black Lion Statuette, and 2 common items. Each chest has a chance for an extra rare item, including exclusive skins not available anywhere else. Preview available.");
		expected.setType(ItemType.Container);
		expected.setLevel(0);
		expected.setRarity(Rarity.Fine);
		expected.setVendorValue(0);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(20316);
		expected.setChatLink("[&AgFcTwAA]");
		expected.setIcon("https://render.guildwars2.com/file/48E4CEEAEEF8F3419A63D4F6295AB77136B86656/711974.png");
		ContainerType expectedType = ContainerType.OpenUI;
		Class<Container> expectedClass = Container.class;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i<result.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Container resultContainer = (Container) result.getDetails();
		assertEquals(expectedType, resultContainer.getType());
	}
	
	@Test
	public void testContainer3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(containerResource + "/test3.json");
		String containerJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(containerJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Iboga Petals");
		expected.setDescription("Double-click to open.");
		expected.setType(ItemType.Container);
		expected.setLevel(80);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(264);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.AccountBound, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(85213);
		expected.setChatLink("[&AgHdTAEA]");
		expected.setIcon("https://render.guildwars2.com/file/3EE5C9740E0A06040F9F5C9DA402D72BEFC8C43C/1765770.png");
		ContainerType expectedType = ContainerType.Immediate;
		Class<Container> expectedClass = Container.class;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i<result.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i<result.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Container resultContainer = (Container) result.getDetails();
		assertEquals(expectedType, resultContainer.getType());
	}

}
