package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.common.GameTypes;

import org.junit.jupiter.api.Test;

public class GenericItemTest
{
	private String genericResource = "GenericItemJson";
	private ItemMapper mapper;
	
	public GenericItemTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testGenericItem1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(genericResource + "/test1.json");
		String genericJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(genericJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Charged Core");
		expected.setType(ItemType.CraftingMaterial);
		expected.setLevel(0);
		expected.setRarity(Rarity.Rare);
		expected.setVendorValue(42);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(24304);
		expected.setChatLink("[&AgHwXgAA]");
		expected.setIcon("https://render.guildwars2.com/file/3903CB74F8D64657FFCCAADB934AF42DFB941E6C/66957.png");
	
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
	}
	
	@Test
	public void testGenericItem2() throws IOException
	{

		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(genericResource + "/test2.json");
		String genericJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(genericJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Chipped Fang");
		expected.setType(ItemType.Trophies);
		expected.setLevel(0);
		expected.setRarity(Rarity.Junk);
		expected.setVendorValue(16);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.SoulbindOnAcquire, Flags.SoulBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(19540);
		expected.setChatLink("[&AgFUTAAA]");
		expected.setIcon("https://render.guildwars2.com/file/D8B93E73503C4D1B36AFA5482A63D1457409EC02/866792.png");
	
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
	}
	
	@Test
	public void testGenericItem3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(genericResource + "/test3.json");
		String genericJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(genericJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Superior Rune of Holding");
		expected.setType(ItemType.TraitGuides);
		expected.setLevel(0);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(12500);
		GameTypes[] gameModes = {GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE, GameTypes.PvP};
		expected.setGameTypes(gameModes);
		Flags[] flags = {};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(13009);
		expected.setChatLink("[&AgHRMgAA]");
		expected.setIcon("https://render.guildwars2.com/file/CFA856D1EE9F01056490EE1CD2C637C2C1C5CBA6/63501.png");
	
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i< expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
	}
}
