package org.guild.wars.objects.items;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;

import org.junit.jupiter.api.Test;

public class MiniaturesTest
{
	private ItemMapper mapper;
	private String miniaturesResource = "MiniatureJson";
	
	public MiniaturesTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testMiniatures1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(miniaturesResource + "/test1.json");
		String miniaturesJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(miniaturesJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Mini Liadri the Concealing Dark");
		expected.setDescription("Double-click to summon this mini to follow you around. Only one mini may be in use at a time.");
		expected.setType(ItemType.Miniatures);
		expected.setLevel(0);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(50);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSell, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(44641);
		expected.setChatLink("[&AgFhrgAA]");
		expected.setIcon("https://render.guildwars2.com/file/959E7AFBACA7951D1104C8C56F63A5BF0566C408/619595.png");
		
		Class<Miniature> expectedClass = Miniature.class;
		int expectedId = 170;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Miniature resultMini = (Miniature) result.getDetails();
		assertEquals(expectedId, resultMini.getId());
	}
	
	@Test
	public void testMiniatures2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(miniaturesResource + "/test2.json");
		String miniaturesJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(miniaturesJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Mini Castor");
		expected.setDescription("Double-click to summon this mini to follow you around. Only one mini may be in use at a time.");
		expected.setType(ItemType.Miniatures);
		expected.setLevel(0);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(50);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoMysticForge, Flags.NoSalvage, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(70703);
		expected.setChatLink("[&AgEvFAEA]");
		expected.setIcon("https://render.guildwars2.com/file/A70E1F1FB13DFFEFD3DECB12750CAB5C7C0EB55F/1191940.png");
		
		Class<Miniature> expectedClass = Miniature.class;
		int expectedId = 349;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Miniature resultMini = (Miniature) result.getDetails();
		assertEquals(expectedId, resultMini.getId());
	}
	
	@Test
	public void testMiniatures3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(miniaturesResource + "/test3.json");
		String miniaturesJson = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(miniaturesJson, Item.class);
		
		Item expected = new Item();
		expected.setName("Mini Plush Griffon");
		expected.setDescription("Double-click to summon this mini to follow you around. Only one mini may be in use at a time.");
		expected.setType(ItemType.Miniatures);
		expected.setLevel(0);
		expected.setRarity(Rarity.Masterwork);
		expected.setVendorValue(50);
		GameTypes[] gameModes = {GameTypes.PvPLobby, GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.AccountBound, Flags.NoSell, Flags.DeleteWarning, Flags.AccountBindOnUse};
		expected.setFlags(flags);
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setId(38455);
		expected.setChatLink("[&AgE3lgAA]");
		expected.setIcon("https://render.guildwars2.com/file/9A7507807FC264B576932BC3CD0AB257CFD54404/526321.png");
		
		Class<Miniature> expectedClass = Miniature.class;
		int expectedId = 119;
		
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i = 0; i < expected.getGameTypes().length; i++)
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i = 0; i < expected.getFlags().length; i++)
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(expectedClass, result.getDetails().getClass());
		
		Miniature resultMini = (Miniature) result.getDetails();
		assertEquals(expectedId, resultMini.getId());
	}
}
