package org.guild.wars.objects.items;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.guild.wars.objects.items.common.Flags;
import org.guild.wars.objects.items.common.GameTypes;
import org.guild.wars.objects.items.common.InfixInfusion;
import org.guild.wars.objects.items.common.InfusionSlots;
import org.guild.wars.objects.items.common.ItemType;
import org.guild.wars.objects.items.common.Rarity;
import org.guild.wars.objects.items.common.Restrictions;
import org.guild.wars.objects.items.mapper.ItemMapper;
import org.guild.wars.objects.items.weapons.DamageType;
import org.guild.wars.objects.items.weapons.WeaponType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * 
 * @author Gab
 * A junit test suite for testing weapons.
 */

public class WeaponTest
{
	private String weaponResource = "WeaponJson";
	private ItemMapper mapper;
	
	public WeaponTest()
	{
		mapper = new ItemMapper();
	}
	
	@Test
	public void testWeapon1() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(weaponResource + "/test1.json");
		String weapon = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(weapon, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Weapon);
		expected.setName("Strong Soft Wood Longbow of Fire");
		expected.setLevel(44);
		expected.setDescription("");
		expected.setRarity(Rarity.Masterwork);
		expected.setVendorValue(120);
		expected.setDefaultSkin(3942);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.Dungeon, GameTypes.PvE, GameTypes.WvW};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.SoulBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(28445);
		expected.setChatLink("[&AgEdbwAA]");
		expected.setIcon("https://render.guildwars2.com/file/C6110F52DF5AFE0F00A56F9E143E9732176DDDE9/65015.png");
		
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(Weapon.class, result.getDetails().getClass());
		
		Weapon resultWeapon = (Weapon) result.getDetails();
		
		WeaponType expectedWeapon = WeaponType.LongBow;
		DamageType expectedDamageType = DamageType.Physical;
		int expectedMin = 385;
		int expectedMax = 452;
		int expectedDef = 0;
		InfusionSlots[] expectedInfusion = {};
		int expectedAttributeLength = 2;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 62;
		AttributeName expectedAttribute2 = AttributeName.Precision;
		int expectedModifier2 = 44;
		
		int expectedSuffixId = 24547;
		String expectedSecondary = "";
		
		assertEquals(expectedWeapon, resultWeapon.getType());
		assertEquals(expectedDamageType, resultWeapon.getDamageType());
		assertEquals(expectedMin, resultWeapon.getMinPower());
		assertEquals(expectedMax, resultWeapon.getMaxPower());
		assertEquals(expectedDef, resultWeapon.getDefense());
		assertEquals(expectedInfusion.length, resultWeapon.getInfusionSlots().length);
		assertEquals(expectedAttributeLength, resultWeapon.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultWeapon.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultWeapon.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultWeapon.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultWeapon.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedSuffixId, resultWeapon.getSuffixItemId());
		assertEquals(expectedSecondary, resultWeapon.getSecondarySuffixItemId());
	}
	
	@Test
	public void testWeapon2() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(weaponResource + "/test2.json");
		String weapon = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(weapon, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Weapon);
		expected.setName("Carrion Legionnaire Short Bow of Rage");
		expected.setLevel(80);
		expected.setRarity(Rarity.Exotic);
		expected.setVendorValue(396);
		expected.setDefaultSkin(5068);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.SoulBindOnUse};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(32310);
		expected.setChatLink("[&AgE2fgAA]");
		expected.setIcon("https://render.guildwars2.com/file/F64F7A05FDA259A1AC342F03083908FD4EC9B779/62423.png");
		
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(Weapon.class, result.getDetails().getClass());
		
		Weapon resultWeapon = (Weapon) result.getDetails();
		
		WeaponType expectedWeapon = WeaponType.ShortBow;
		DamageType expectedDamageType = DamageType.Physical;
		int expectedMin = 905;
		int expectedMax = 1000;
		int expectedDef = 0;
		InfusionSlots[] expectedInfusion = {};
		int expectedAttributeLength = 3;
		AttributeName expectedAttribute1 = AttributeName.Power;
		int expectedModifier1 = 171;
		AttributeName expectedAttribute2 = AttributeName.Vitality;
		int expectedModifier2 = 171;
		AttributeName expectedAttribute3 = AttributeName.ConditionDamage;
		int expectedModifier3 = 239;
		
		int expectedSuffixId = 24561;
		String expectedSecondary = "";
		
		assertEquals(expectedWeapon, resultWeapon.getType());
		assertEquals(expectedDamageType, resultWeapon.getDamageType());
		assertEquals(expectedMin, resultWeapon.getMinPower());
		assertEquals(expectedMax, resultWeapon.getMaxPower());
		assertEquals(expectedDef, resultWeapon.getDefense());
		assertEquals(expectedInfusion.length, resultWeapon.getInfusionSlots().length);
		assertEquals(expectedAttributeLength, resultWeapon.getInfixUpgrade().getAttributes().length);
		assertEquals(expectedAttribute1, resultWeapon.getInfixUpgrade().getAttributes()[0].getAttribute());
		assertEquals(expectedModifier1, resultWeapon.getInfixUpgrade().getAttributes()[0].getModifier());
		assertEquals(expectedAttribute2, resultWeapon.getInfixUpgrade().getAttributes()[1].getAttribute());
		assertEquals(expectedModifier2, resultWeapon.getInfixUpgrade().getAttributes()[1].getModifier());
		assertEquals(expectedAttribute3, resultWeapon.getInfixUpgrade().getAttributes()[2].getAttribute());
		assertEquals(expectedModifier3, resultWeapon.getInfixUpgrade().getAttributes()[2].getModifier());
		assertEquals(expectedSuffixId, resultWeapon.getSuffixItemId());
		assertEquals(expectedSecondary, resultWeapon.getSecondarySuffixItemId());
	}
	
	@Test
	public void testWeapon3() throws IOException
	{
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(weaponResource + "/test3.json");
		String weapon = IOUtils.toString(inputStream, "utf-8");
		Item result = mapper.readValue(weapon, Item.class);
		Item expected = new Item();
		
		expected.setType(ItemType.Weapon);
		expected.setName("Incinerator");
		expected.setLevel(80);
		expected.setRarity(Rarity.Legendary);
		expected.setVendorValue(100000);
		expected.setDefaultSkin(4682);
		GameTypes[] gameModes = { GameTypes.Activity, GameTypes.WvW, GameTypes.Dungeon, GameTypes.PvE};
		expected.setGameTypes(gameModes);
		Flags[] flags = {Flags.HideSuffix, Flags.NoSalvage, Flags.NoSell, Flags.AccountBindOnUse, Flags.DeleteWarning};
		Restrictions[] restrictions = {};
		expected.setRestrictions(restrictions);
		expected.setFlags(flags);
		expected.setId(30687);
		expected.setChatLink("[&AgHfdwAA]");
		expected.setIcon("https://render.guildwars2.com/file/D9B12A1EBEDC04047295CF0A66E43E650297D429/456012.png");
		
		assertEquals(expected.getType(), result.getType());
		assertEquals(expected.getName(), result.getName());
		assertEquals(expected.getLevel(), result.getLevel());
		assertEquals(expected.getDescription(), result.getDescription());
		assertEquals(expected.getRarity(), result.getRarity());
		assertEquals(expected.getVendorValue(), result.getVendorValue());
		assertEquals(expected.getGameTypes().length, result.getGameTypes().length);
		for(int i =0; i< gameModes.length; i++)
		{
			assertEquals(expected.getGameTypes()[i], result.getGameTypes()[i]);
		}
		assertEquals(expected.getFlags().length, result.getFlags().length);
		for(int i =0; i< flags.length; i++)
		{
			assertEquals(expected.getFlags()[i], result.getFlags()[i]);
		}	
		assertEquals(expected.getRestrictions().length, result.getRestrictions().length);
		assertEquals(expected.getId(), result.getId());
		assertEquals(expected.getChatLink(), result.getChatLink());
		assertEquals(expected.getIcon(), result.getIcon());
		assertEquals(Weapon.class, result.getDetails().getClass());
		
		Weapon resultWeapon = (Weapon) result.getDetails();
		
		WeaponType expectedWeapon = WeaponType.Dagger;
		DamageType expectedDamageType = DamageType.Fire;
		int expectedMin = 970;
		int expectedMax = 1030;
		int expectedDef = 0;
		int expectedInfusionSize = 1;
		InfixInfusion expectedFlag = InfixInfusion.Infusion;
		int expectedSuffixId = 24548;
		String expectedSecondary = "";
		int[] expectedStatChoices = {155,161,159,157,158,160,153,605,700,616,154,156,162,686,
				  559,754,753,799,1026,1067,1123,1140,1085,1153,1118,1131,1111,1109,1222,1344,
				  1363,1364,628,1032,1484,1539 };
		
		assertEquals(expectedWeapon, resultWeapon.getType());
		assertEquals(expectedDamageType, resultWeapon.getDamageType());
		assertEquals(expectedMin, resultWeapon.getMinPower());
		assertEquals(expectedMax, resultWeapon.getMaxPower());
		assertEquals(expectedDef, resultWeapon.getDefense());
		assertEquals(expectedSuffixId, resultWeapon.getSuffixItemId());
		assertEquals(expectedSecondary, resultWeapon.getSecondarySuffixItemId());
		assertEquals(expectedInfusionSize, resultWeapon.getInfusionSlots().length);
		assertEquals(expectedFlag, resultWeapon.getInfusionSlots()[0].getFlags()[0]);
		assertEquals(expectedStatChoices.length, resultWeapon.getStatChoices().length);
		for(int i = 0; i < expectedStatChoices.length; i++)
			assertEquals(expectedStatChoices[i], resultWeapon.getStatChoices()[i]);
	}
}
